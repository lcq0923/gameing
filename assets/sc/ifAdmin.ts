import { _decorator, Component, Node } from 'cc'
const { ccclass, property } = _decorator

@ccclass('ifAdmin')
export class ifAdmin extends Component {
  private yes = false
  onLoad() {
    this.node.active = this.yes
  }

  update(deltaTime: number) {}
}
