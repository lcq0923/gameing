import {
  _decorator,
  Component,
  Node,
  Vec3,
  game,
  Tween,
  tween,
  input,
  Input,
  EventTouch,
  Camera,
  PhysicsSystem,
  geometry,
  Prefab,
  instantiate,
  EventMouse,
  EditBox,
  resources,
  Slider,
  Toggle,
  find,
  Label,
  Vec2,
  math,
  CCObject,
  director,
  EventKeyboard,
  sys,
  AudioClip,
  AudioSource,
  Pool,
  BatchingUtility,
  JsonAsset,
  BoxCollider,
  SkeletalAnimation,
  Light,
  DirectionalLight,
  CCString,
  CCBoolean,
} from 'cc'
import { coin } from './coin'
import { joAlert } from './joAlert'
import { coinPool } from './coinPool'
import { ttRecord } from './ttRecord'
import { gb } from './gb'
import { rank } from './rank'
const { ccclass, property } = _decorator

/**
 * Predefined variables
 * Name = camera
 * DateTime = Fri Sep 16 2022 17:06:57 GMT+0800 (中国标准时间)
 * Author = minijoe
 * FileBasename = camera.ts
 * FileBasenameNoExtension = camera
 * URL = db://assets/sc/camera.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */

@ccclass('camera')
export class camera extends Component {
  private activeQuee: Array<Node> = []
  private panTop = 5
  private panDown = -4
  private panLeft = -3
  private panRight = 3
  /**
   * vivo视频广告id
   */
  @property({ displayName: 'vivo视频广告id' })
  vv_adid = ''
  @property({ displayName: '微信banner广告id' })
  wx_bannerid = 'adunit-aa103d89693d3ad8'
  @property({ displayName: '微信插屏广告id' })
  interstitialAdId = 'adunit-aab7ae4f63d89b4a'
  @property({ displayName: '是否显示底部微信广告' })
  showBottomWxBanner = false

  @property({ displayName: '微信插屏每隔几秒显示一次，-1为不显示' })
  interstitialAdInter = 30
  /**
   * 微信视频广告id
   */
  @property({ displayName: '微信视频广告id' })
  wx_adid = ''
  /**
   * 字节视频广告id
   */
  @property({ displayName: '字节视频广告id' })
  tt_adid = '11g1725ibi08a1ca9e'
  /**
   * 是否使用广告来激励，如果为false,则用分享激励，wx_adid在为空时这个会强制为false
   */
  @property({
    displayName: '是否用微信视频广告替代分享',
    tooltip: '微信小游戏中道具使用时选择分享回调或视频回调',
  })
  wxUseAd = true

  @property({
    displayName: '微信下设置是否随机用分享或广告进行激励回调',
    tooltip: '如果为真，那么在设置微信视频广告替代分享会变为随机',
  })
  wxShareAdRand = false

  @property({
    displayName: '是否网络版',
    tooltip: '单机版请关闭，不然会有报错',
  })
  isRemoteVersion = false

  @property({ displayName: '分享文案', type: [CCString] })
  shareTitle = ['羊了个羊3D版等你挑战！', '下一个消除达人是你吗？']

  @property({ displayName: '分享文案随机' })
  shareTitleRand = true

  @property({ displayName: '直接分享图', type: [CCString] })
  shareImgUrlNormal = ['https://cdn.wxnodes.cn/lv/share.jpg']

  @property({ displayName: '朋友圈分享图', type: [CCString] })
  shareImgUrlTimeline = ['']

  @property({ displayName: '分享图随机' })
  shareImgRand = true

  @property(Node)
  rankBtn = new Node()
  @property(Node)
  toolBtnWrap = new Node()
  @property(rank)
  rankPop
  @property(Prefab)
  effectNode
  @property(joAlert)
  myAlert: joAlert
  @property(Camera)
  readonly cameraCom!: Camera
  private _ray: geometry.Ray = new geometry.Ray()
  private _ray2: geometry.Ray = new geometry.Ray()
  private _ray3: geometry.Ray = new geometry.Ray()
  @property(Node)
  playEditBtn = new Node()
  @property(Node)
  UIMake = new Node()
  @property(Node)
  UIPlay = new Node()

  @property(DirectionalLight)
  light
  @property(Node)
  nodeWrap = new Node()
  @property(Node)
  getQuessBtn = new Node()
  @property(Node)
  targetDesk = new Node()

  @property(CCBoolean)
  hd = false
  @property(Node)
  character = new Node()
  @property(Node)
  deleteArea = new Node()
  @property(Node)
  panOutWrap = new Node()
  @property(Node)
  desk = new Node()

  @property([Prefab])
  nodeprbs: (Prefab | null)[] = []

  private tmp: Node
  private lastModifyCoin: Node
  private moving = false
  public floory = 12.414
  public floorx = -9.354
  public floorz = -17.514
  public pery = 0.522
  public perCeil = 2.5

  @property(Slider)
  zindex: Slider = new Slider()
  //偏移
  @property(AudioClip)
  btnAu
  @property(AudioClip)
  yesAu
  @property(AudioClip)
  bulingAu
  @property(Toggle)
  zpian: Toggle = new Toggle()
  @property(Toggle)
  xpian: Toggle = new Toggle()

  @property(Toggle)
  ifRandInit: Toggle = new Toggle()
  @property(Toggle)
  ifHD: Toggle = new Toggle()

  @property(Toggle)
  ifSlient: Toggle = new Toggle()
  @property(Toggle)
  underNoMove: Toggle = new Toggle()

  @property(Node)
  holderCeil = new Node()
  @property(Node)
  holderHover = new Node()
  @property(Node)
  holderBox = new Node()

  @property(EditBox)
  cat = new EditBox()

  @property(EditBox)
  total = new EditBox()

  @property(Node)
  initBtn = new Node()

  @property(Node)
  panCoinsWrap = new Node()

  @property(Node)
  deskBlank = new Node()
  @property(Node)
  slideCamera = new Node()

  @property(Label)
  randLimitLabel = new Label()
  @property
  _randLimit = 1
  @property
  public get randLimit() {
    return this._randLimit
  }
  public set randLimit(value) {
    this._randLimit = value
    this.randLimitLabel.string = '' + value
  }

  @property(Label)
  pullLimitLabel = new Label()
  @property
  _pullLimit = 1
  @property
  public get pullLimit() {
    return this._pullLimit
  }
  public set pullLimit(value) {
    this._pullLimit = value
    this.pullLimitLabel.string = '' + value
  }

  @property
  _aliveLimit = 2
  @property
  public get aliveLimit() {
    return this._aliveLimit
  }
  public set aliveLimit(value) {
    this._aliveLimit = value
  }

  @property(Label)
  redoLimitLabel = new Label()
  @property
  _redoLimit = 1
  @property
  public get redoLimit() {
    return this._redoLimit
  }
  public set redoLimit(value) {
    this._redoLimit = value
    this.redoLimitLabel.string = '' + value
  }

  @property
  _score = 0
  @property //金币get/set方法
  public get score() {
    return this._score
  }
  public set score(value) {
    this._score = value
    this.scoreLabel.string = '得分：' + value

    sys.localStorage.setItem(
      'maxScore',
      Math.max(parseInt(sys.localStorage.getItem('maxScore') || '0'), value) +
      '',
    )
  }

  @property(Label)
  scoreLabel = new Label()

  @property(Label)
  quessLabel = new Label()

  public scene = 'make'
  eidtBtn() {
    this.scene = 'make'
    this.init()
  }
  playBtn() {
    this.scene = 'play'
    var self = this
    this.myAlert.open({
      content: '放弃保存试玩当前关卡吗？',
      needCancel: true,
      confirm() {
        self.init()
      },
    })
  }
  @property(Slider)
  cameraSlider: Slider = new Slider()
  cameraSlide(e) {
    var len = new Vec3(this.orgPos.x, this.orgPos.y, this.orgPos.z).subtract(
      this.slideCamera.position,
    )

    this.node.position = new Vec3(
      this.orgPos.x,
      this.orgPos.y - this.cameraSlider.progress * len.y,
      this.orgPos.z - this.cameraSlider.progress * len.z,
    )
  }
  private maxZindex = 100
  private aus: AudioSource
  au(clip, volume = 1) {
    var aus = this.aus || new AudioSource()
    aus.clip = clip
    aus.volume = volume
    aus.playOneShot(clip, 2)
  }
  /**
   * 生成关卡数据
   */
  getQuessData() {
    var array = this.nodeWrap.children
    var ds = []
    for (let index = 0; index < array.length; index++) {
      const element = array[index]
      //记录一个数据
      var d = {
        position: {
          x: element.position.x,
          y: element.position.y,
          z: element.position.z,
        },
        name: element.name,
        ifUnder: element.getComponent(coin).isUnder,
        zindex: (element as any).zindex,
        xpian: (element as any).xpian,
        zpian: (element as any).zpian,
      }
      ds.push(d)
    }
    //console.log(JSON.stringify(ds))
    this.quessData[this.nowQuessIndex] = ds
    this.saveDataRemote((d) => {
      alert('保存成功')
    })
  }
  //
  getDataRermote(cb) {
    resources.load('json/data', (err, d) => {
      if (err) {
        console.log(err)

        return
      }
      var data = (d as JsonAsset).json
      if (sys.localStorage.getItem('data')) {
        data = JSON.parse(sys.localStorage.getItem('data'))
      }
      cb(data)
    })
  }
  saveDataRemote(cb) {
    sys.localStorage.setItem('data', JSON.stringify(this.quessData))
    if (cb) cb()
  }
  /**
   * 随机抽出
   * @param arrList
   * @param num
   * @returns
   */
  makeRandomArr(arrList, num) {
    if (num > arrList.length) {
      return
    }
    //    var tempArr=arrList.concat();
    var tempArr = arrList.slice(0)
    var newArrList = []
    for (var i = 0; i < num; i++) {
      var random = Math.floor(Math.random() * (tempArr.length - 1))
      var arr = tempArr[random]
      tempArr.splice(random, 1)
      newArrList.push(arr)
    }
    return newArrList
  }
  rePanPos(cb = null) {
    var array = this.panCoinsWrap.children
    for (let index = 0; index < array.length; index++) {
      const element = array[index]
      //   element.setPosition(new Vec3(-7.255 + this.perCeil * index, 0))
      //   if (cb) cb()
      tween(element)
        .to(0.2, { position: new Vec3(-7.255 + this.perCeil * index, 0) })
        .call(() => {
          if (cb) cb()
        })
        .start()
    }
  }
  private lastFlyData
  flyTo(node: Node, index, cb = null) {
    var holder = find('holder/Node', this.panCoinsWrap.parent)
    var p = new Vec3(-7.255 + this.perCeil * index, 0)
    holder.setPosition(p)

    var pos = holder.getWorldPosition()

    var from = new Vec3(
      node.worldPosition.x,
      node.worldPosition.y,
      node.worldPosition.z,
    )
    var to = new Vec3(pos.x, pos.y, pos.z)
    var localFrom = new Vec3(node.position.x, node.position.y, node.position.z)
    tween(node)
      .to(0.1, { worldPosition: pos })
      .delay(0.02)
      .call(() => {
        node.parent = this.panCoinsWrap
        node.position = p
        node.setSiblingIndex(index)
        this.lastFlyData = {
          node: node,
          from: from,
          localFrom: localFrom,
          to: to,
        }
        if (cb) cb()
      })
      .start()
  }
  /**
   * 生成空位
   * @param i
   */
  makeBlank(i) {
    var array = this.panCoinsWrap.children
    for (let index = 0; index < array.length; index++) {
      if (index >= i) {
        const element = array[index]
        element.setPosition(element.position.add(new Vec3(this.perCeil, 0, 0)))
        //element.setSiblingIndex(element.getSiblingIndex() + 1)
      }
    }
  }

  findIndex(name) {
    var array = this.panCoinsWrap.children
    var i = array.length
    for (let index = 0; index < array.length; index++) {
      const element = array[index]
      if (element.name == name) {
        i = index + 1
      }
    }
    return i
  }
  private gamestatus = 'playing'
  /**
   * play模式下游戏结束
   */
  gamelost(n) {
    if (this.gamestatus == 'over') return
    this.gamestatus = 'over'
    // if (typeof tt != 'undefined') this.node.getComponent(ttRecord).recordStop()
    var self = this
    this.myAlert.open({
      title: '提示',
      content: '你失败了',
      confirmTxt: self.aliveLimit > 0 ? '我再试试' : '重开本关',
      cancelTxt: '分享录屏',
      needCancel: false,
      ifCancelShut: false,
      confirm() {
        if (self.wxShareAdRand) {
          //设置用道具时随机用分享或广告回调
          self.wxUseAd = self.randomNum(0, 1) == 0
        }
        if (
          typeof wx != 'undefined' &&
          typeof tt == 'undefined' &&
          self.aliveLimit > 0 &&
          !self.wxUseAd
        ) {
          self.myAlert.open({
            title: '提示',
            content: '选择再次尝试模式',
            confirmTxt: '分享复活',
            cancelTxt: '直接重开',
            needCancel: true,
            ifConfirmShut: false,
            confirm(e, aler) {
              self.regWxShareEvent(() => {
                self.aliveLimit--
                self.gamestatus = 'playing'
                self.pull({}, true)
                aler.shut()
              })
            },
            cancel() {
              self.gamestatus = 'playing'
              self.reloadQuess(0, true)
            },
          })
        } else if (
          (typeof tt != 'undefined' &&
            typeof wx != 'undefined' &&
            self.aliveLimit > 0) ||
          (typeof wx != 'undefined' &&
            typeof tt == 'undefined' &&
            self.aliveLimit > 0 &&
            self.wxUseAd) ||
          (typeof qg != 'undefined' && self.aliveLimit > 0)
        ) {
          self.myAlert.open({
            title: '提示',
            content: '选择再次尝试模式',
            confirmTxt: '复活',
            cancelTxt: '直接重开',
            needCancel: true,
            ifConfirmShut: false,
            confirmBtnIsVideo: true,
            confirm(e, aler) {
              self.gbShowVideoAd({
                success() {
                  self.aliveLimit--
                  self.gamestatus = 'playing'
                  self.pull({}, true)
                  aler.shut()
                },
              })
            },
            cancel() {
              self.gamestatus = 'playing'
              self.reloadQuess(0, true)
            },
          })
        } else {
          self.gamestatus = 'playing'
          self.reloadQuess(0, true)
          // self.gamestatus = 'playing'
          // self.pull({}, true)
          //aler.shut()
        }
      },
      cancel() {
        self.node.getComponent(ttRecord).share()
      },
    })
  }
  /**
   * play模式下游戏结束
   */
  gamewin() {
    console.log('win')
    //上报分数
    if (this.isRemoteVersion) {
      gb.r({
        name: 'saveMax',
        method: 'post',
        data: {
          score: this.score,
        },
        success(e) {
          console.log(e)
        },
      })
    }

    //this.yesAu.play()
    this.au(this.yesAu, 1)
    if (this.gamestatus == 'over') return
    this.gamestatus = 'over'

    // if (typeof tt != 'undefined') this.node.getComponent(ttRecord).recordStop()
    var self = this
    if (!this.quessData[this.nowQuessIndex + 1]) {
      //通关了
      console.log('sdfs')

      director.loadScene('end')
    } else {
      this.myAlert.open({
        title: '提示',
        content: '过关了',
        confirmTxt: '下一关',
        cancelTxt: '分享录屏',
        needCancel: false,
        ifCancelShut: false,
        confirm() {
          self.gamestatus = 'playing'
          self.reloadQuess(1)
        },
        cancel() {
          self.node.getComponent(ttRecord).share()
        },
      })
    }
  }
  /**
   *点击
   */
  private clickingCoin = false

  makeMakeQuess() {
    var data = this.quessData[this.nowQuessIndex]
    if (!data) {
      console.log('不存在关卡')

      return
    }
    this.quessLabel.string = '编辑第' + (this.nowQuessIndex + 1) + '关'
    for (let index = 0; index < data.length; index++) {
      const element = data[index]
        ; (() => {
          resources.load('coins/' + element.name, (err, p) => {
            if (err) {
              //console.log(err);
              // if (cb)
              //     cb()
              return
            }
            var n = instantiate(p as Prefab)
            n.position = element.position
              ; (n as any).zindex = element.zindex
              ; (n as any).zpian = element.zpian
              ; (n as any).xpian = element.xpian
            n.parent = this.nodeWrap
          })
        })()
    }

    this.initPanDataByData(data)
    return true
  }
  /**
   * 根据数据布局
   *
   */
  public playMaxY = 0
  makeQuess(rand = true, cb = null) {
    var ds = this.quessData[this.nowQuessIndex]
    //console.log(ds)

    if (!ds) {
      console.log('不存在关卡')
      return
    }
    ds = JSON.parse(JSON.stringify(ds))
    var ns = []
    //把名字取出来，乱序
    if (rand) {
      for (let index = 0; index < ds.length; index++) {
        ns.push(ds[index].name)
      }
      ns.sort(function () {
        return 0.5 - Math.random()
      })
    }

    this.quessLabel.string = '第' + (this.nowQuessIndex + 1) + '关'
    // this.nodeWrap.destroyAllChildren()
    // this.panCoinsWrap.destroyAllChildren()
    this.nodeDestroyAllPoolNode(this.nodeWrap)
    this.nodeDestroyAllPoolNode(this.panCoinsWrap)
    var loadNum = 0
    for (let index = 0; index < ds.length; index++) {
      var d = ds[index]
      var name = d.name
      if (rand) {
        name = ns[index]
      }
      ; ((d, name) => {
        if (d.position.y > this.playMaxY) {
          this.playMaxY = d.position.y
        }
        var nnn = this.node.getComponent(coinPool).createNode(name)
        var nn = nnn.node
        this.activeQuee.push(nn)
        nn.active = false
        nn.position = new Vec3(d.position.x, d.position.y, d.position.z)
        nn.name = name

        nn.getComponent(coin).isUnder = d.ifUnder || false
        nn.getComponent(coin).isFly = false
        nn.getComponent(coin).static = false
        nn.scale = new Vec3(1.28, 1.28, 1.28)
        //nn.active = true
        nn.parent = this.nodeWrap
        nn.on(
          'DIYTOUCHSTART-THROUGH',
          () => {
            if (this.gamestatus == 'over') return
            if (this.clickingCoin) return
            var c = nn.getComponent(coin)
            //console.log('click', c.node.name, c.isUnder)
            //this.btnAu.play()

            if (!c.isUnder && !c.isFly) {
              this.clickingCoin = true
              this.au(this.btnAu, 1)
              var i = this.findIndex(c.node.name)
              //console.log(i, c.node.name)
              c.isFly = true
              this.makeBlank(i)
              this.flyTo(c.node, i, () => {
                //如果超过7个了，gameover

                var i = c.node.getSiblingIndex()

                if (
                  c.node.parent.children[i - 1] &&
                  c.node.parent.children[i - 2] &&
                  c.node.parent.children[i - 1].name == c.node.name &&
                  c.node.parent.children[i - 2].name == c.node.name
                ) {
                  //可以消除
                  this.lastFlyData = false
                  var fst = c.node.parent.children[i - 2],
                    sec = c.node.parent.children[i - 1],
                    thd = c.node.parent.children[i]

                  var self = this

                  if (this.effectNode) {
                    var e1 = instantiate(this.effectNode)

                    var e2 = instantiate(this.effectNode)

                    var e3 = instantiate(this.effectNode)

                    e1.parent = this.holderHover.parent
                    e2.parent = this.holderHover.parent
                    e3.parent = this.holderHover.parent

                    e1.setWorldPosition(fst.getWorldPosition())
                    e2.setWorldPosition(sec.getWorldPosition())
                    e3.setWorldPosition(thd.getWorldPosition())
                    tween(e1)
                      .delay(0.5)
                      .call(() => {
                        e1.destroy()
                        e2.destroy()
                        e3.destroy()
                      })
                      .start()
                  }

                  var hasAct = false
                  function act() {
                    //self.bulingAu.play()
                    self.au(self.bulingAu, 1)
                    if (hasAct) return
                    hasAct = true
                    self.clickingCoin = false
                    self.rePanPos(() => { })
                    if (
                      self.panCoinsWrap.children.length <= 0 &&
                      self.nodeWrap.children.length <= 0
                    ) {
                      self.gamewin()
                      return
                    }
                    if (
                      self.panCoinsWrap.children.length >= 7 &&
                      self.nodeWrap.children.length > 0
                    ) {
                      self.gamelost(1)
                      return
                    }
                  }
                  var n = 0
                  tween(fst)
                    .to(0.1, { scale: new Vec3(0, 0, 0) })
                    .call(() => {
                      fst.removeFromParent()
                      this.node.getComponent(coinPool).destoryNode(fst)
                      n++
                      if (n >= 3) {
                        act()
                      }
                    })
                    .start()
                  tween(sec)
                    .to(0.12, { scale: new Vec3(0, 0, 0) })
                    .call(() => {
                      sec.removeFromParent()
                      this.node.getComponent(coinPool).destoryNode(sec)
                      n++
                      if (n >= 3) {
                        act()
                      }
                    })
                    .start()
                  tween(thd)
                    .to(0.12, { scale: new Vec3(0, 0, 0) })
                    .call(() => {
                      thd.removeFromParent()
                      this.node.getComponent(coinPool).destoryNode(thd)
                      n++
                      if (n >= 3) {
                        act()
                      }
                    })
                    .start()
                  //this.score += fst.getComponent(coin).score
                  this.score += 10
                } else {
                  this.clickingCoin = false
                  if (
                    this.panCoinsWrap.children.length <= 0 &&
                    this.nodeWrap.children.length <= 0
                  ) {
                    this.gamewin()
                    return
                  }
                  if (
                    this.panCoinsWrap.children.length >= 7 &&
                    this.nodeWrap.children.length > 0
                  ) {
                    this.gamelost(2)
                    return
                  }
                }
              })

              //马上触发底下node的checkunder
              var array = nn.getComponent(coin).getUnderPengNode()
              for (let index = 0; index < array.length; index++) {
                const element = array[index]
                element.getComponent(coin).isUnder = element
                  .getComponent(coin)
                  .checkIfUnder()
              }
            }
          },
          this,
        )

        //if (nnn.isNews) {
        nn.on('COINSTART', () => {
          loadNum++
          if (loadNum >= ds.length) {
            if (cb) cb()
          }
        })
      })(d, name)
    }
    return true
  }
  nodeDestroyAllPoolNode(p) {
    var array = p.children
    var i = 0
    while (array.length > 0) {
      this.node.getComponent(coinPool).destoryNode(array[array.length - 1])
      i++
    }
  }
  /**
   * 乱排棋盘上的
   */
  randCoins() {
    //this.btnAu.play()
    this.au(this.btnAu, 1)
    if (this.randLimit == 0) {
      this.myAlert.open({
        title: '提示',
        content: '只能使用一次',
        confirmTxt: 'OK',
        confirm() { },
        cancel() { },
      })
      return
    }
    if (this.nodeWrap.children.length == 0) {
      this.myAlert.open({
        title: '提示',
        content: '暂不需要用到',
        confirmTxt: 'OK',
        confirm() { },
        cancel() { },
      })
      return
    }

    var act = () => {
      this.randLimit = 0
      var coins = this.nodeWrap.children
      for (let x = 0; x < coins.length; x++) {
        const element1 = coins[x]
        //随机选一个交换位置
        const randomEl = coins[this.randomNum(0, coins.length - 1)]
        var p1 = new Vec3(
          element1.worldPosition.x,
          element1.worldPosition.y,
          element1.worldPosition.z,
        )
        var p2 = new Vec3(
          randomEl.worldPosition.x,
          randomEl.worldPosition.y,
          randomEl.worldPosition.z,
        )
        var u1 = element1.getComponent(coin).isUnder
        var u2 = randomEl.getComponent(coin).isUnder

        element1.getComponent(coin).isUnder = u2
        randomEl.getComponent(coin).isUnder = u1
        element1.worldPosition = p2
        randomEl.worldPosition = p1
      }
    }
    if (this.wxShareAdRand) {
      //设置用道具时随机用分享或广告回调
      this.wxUseAd = this.randomNum(0, 1) == 0
    }
    if (
      typeof tt != 'undefined' ||
      (typeof wx != 'undefined' && this.wxUseAd) ||
      typeof qg != 'undefined'
    ) {
      var self = this
      this.myAlert.open({
        content: '随机打乱水果币',
        needCancel: true,
        cancelTxt: '不，谢谢',
        confirmTxt: '获取',
        confirmBtnIsVideo: true,
        confirm() {
          self.gbShowVideoAd({
            success() {
              act()
            },
          })
        },
      })
    } else {
      if (typeof wx != 'undefined') {
        var self = this
        self.myAlert.open({
          content: '随机打乱水果币',
          needCancel: true,
          cancelTxt: '不，谢谢',
          confirmTxt: '分享获取',
          confirm() {
            self.regWxShareEvent(act)
          },
        })
      } else act()
    }
  }
  regWxShareEvent(cb) {
    if (typeof wx != 'undefined') {
      var title = this.shareTitle[0]
      if (this.shareTitleRand)
        title = this.shareTitle[this.randomNum(0, this.shareTitle.length - 1)]
      var url = this.shareImgUrlNormal[0]
      if (this.shareImgRand)
        url = this.shareImgUrlNormal[
          this.randomNum(0, this.shareImgUrlNormal.length - 1)
        ]
      wx.shareAppMessage({
        title: title,
        imageUrl: url,
      })
      wx.tmpAct = act
      wx.tmpTime = Date.parse(new Date().toString())
    }
    var self = this
    function act() {
      if (typeof wx != 'undefined') {
        if (Date.parse(new Date().toString()) - wx.tmpTime < 2000) {
          wx.tmpAct = null
          return
        }
      }
      setTimeout(() => {
        if (cb) cb()
      }, 200)
    }
  }
  /**
   * 返回上一步
   */
  redo(e) {
    //this.btnAu.play()
    this.au(this.btnAu, 1)
    if (this.redoLimit == 0) {
      this.myAlert.open({
        title: '提示',
        content: '只能使用一次',
        confirmTxt: 'OK',
        confirm() { },
        cancel() { },
      })
      return
    }
    console.log(this.lastFlyData)

    if (!this.lastFlyData) {
      this.myAlert.open({
        title: '提示',
        content: '暂不需要用到',
        confirmTxt: 'OK',
        confirm() { },
        cancel() { },
      })
      return
    }

    var self = this
    function act() {
      console.log('测试')

      self.redoLimit = 0

      tween(self.lastFlyData.node)
        .to(0.3, { worldPosition: self.lastFlyData.from })
        .call(() => {
          self.lastFlyData.node.parent = self.nodeWrap
          self.lastFlyData.node.position = self.lastFlyData.localFrom
          self.lastFlyData.node.getComponent(coin).isFly = false

          var array = self.lastFlyData.node
            .getComponent(coin)
            .getUnderPengNode()
          for (let index = 0; index < array.length; index++) {
            const element = array[index]
            element.getComponent(coin).isUnder = true
          }
          self.lastFlyData.node.getComponent(coin).isUnder = false
          self.lastFlyData = null
          self.rePanPos()
        })
        .start()
    }
    if (this.wxShareAdRand) {
      //设置用道具时随机用分享或广告回调
      this.wxUseAd = this.randomNum(0, 1) == 0
    }
    if (
      typeof tt != 'undefined' ||
      (typeof wx != 'undefined' && this.wxUseAd) ||
      typeof qg != 'undefined'
    ) {
      var self = this
      this.myAlert.open({
        content: '撤销上一次点击，并放置原位',
        needCancel: true,
        cancelTxt: '不，谢谢',
        confirmTxt: '获取',
        confirmBtnIsVideo: true,
        confirm() {
          self.gbShowVideoAd({
            success() {
              act()
            },
          })
        },
      })
    } else {
      if (typeof wx != 'undefined') {
        var self = this
        this.myAlert.open({
          content: '撤销上一次点击，并放置原位',
          needCancel: true,
          cancelTxt: '不，谢谢',
          confirmTxt: '分享获取',
          confirm() {
            self.regWxShareEvent(act)
          },
        })
      } else act()
    }

    //this.lastFlyData.node.worldPosition=this.lastFlyData.from
  }

  /**
   * 把盘子里的coin挪到自由区
   */
  pull(e, unLimit = false) {
    var self = this
    //this.btnAu.play()
    this.au(this.btnAu, 1)
    //for (let x = this.panLeft; x <= this.panRight; x++) {

    var act = (noSetLimit = false) => {
      if (!noSetLimit) this.pullLimit = 0
      //}
      var array = this.panCoinsWrap.children
      for (let index = 0; index < array.length; index++) {
        const element = array[index]
        var n = new Node()
        n.parent = this.holderCeil.parent
        var loclPos = new Vec3(
          (this.panLeft + index) * this.perCeil,
          this.floory,
          -this.panTop * this.perCeil,
        )
        n.setPosition(loclPos)
        n.active = true
        var pos = n.getWorldPosition()
        //console.log(this.getCoinsByPos(n.position));
        var array2 = self.getCoinsByPos(n.position).all
        if (array2.length > 0) {
          for (let index2 = 0; index2 < array2.length; index2++) {
            const element2 = array2[index2]
            element2.getComponent(coin).isUnder = true
          }
          pos = new Vec3(pos.x, pos.y + self.pery * array2.length, pos.z)
          loclPos = new Vec3(
            loclPos.x,
            loclPos.y + self.pery * array2.length,
            loclPos.z,
          )
        }
        element.getComponent(coin).isFly = false
          ; ((element, pos, loclPos) => {
            tween(element)
              .to(0.3, {
                worldPosition: pos,
              })
              .call(() => {
                element.parent = this.nodeWrap
                element.position = loclPos

                //console.log(loclPos)
              })
              .start()
          })(element, pos, loclPos)
        n.destroy()
        //element.setWorldPosition(pos)
        //element.removeFromParent()

        //console.log(n.position);
      }
    }

    if (unLimit) {
      act(true)
      return
    }

    if (this.pullLimit == 0) {
      this.myAlert.open({
        title: '提示',
        content: '只能使用一次',
        confirmTxt: 'OK',
        confirm() { },
        cancel() { },
      })
      return
    }
    if (this.panCoinsWrap.children.length == 0) {
      this.myAlert.open({
        title: '提示',
        content: '暂不需要用到',
        confirmTxt: 'OK',
        confirm() { },
        cancel() { },
      })
      return
    }
    if (this.wxShareAdRand) {
      //设置用道具时随机用分享或广告回调
      this.wxUseAd = this.randomNum(0, 1) == 0
    }
    if (
      typeof tt != 'undefined' ||
      (typeof wx != 'undefined' && this.wxUseAd) ||
      typeof qg != 'undefined'
    ) {
      var self = this
      this.myAlert.open({
        content: '移出盘子中的水果币',
        needCancel: true,
        cancelTxt: '不，谢谢',
        confirmTxt: '获取',
        confirmBtnIsVideo: true,
        confirm() {
          self.gbShowVideoAd({
            success() {
              act()
            },
          })
        },
      })
    } else {
      if (typeof wx != 'undefined') {
        var self = this
        self.myAlert.open({
          content: '移出盘子中的水果币',
          needCancel: true,
          cancelTxt: '不，谢谢',
          confirmTxt: '分享获取',
          confirm() {
            self.regWxShareEvent(act)
          },
        })
      } else act()
    }
    //this.panCoinsWrap.removeAllChildren()
  }
  /**
   * 随机拆分整数
   * @param n
   * @param x
   * @returns
   */
  intSplit(n, x) {
    if (x < 2) {
      return [n]
    }
    if (n < x) {
      return [n]
    }
    var min = Math.ceil(n / (2 * x))
    var max = Math.floor((2 * n) / x)
    var arr = []
    var random
    while (x > 0) {
      if (x == 1) {
        arr.push(n)
        break
      } else if (max * x == n) {
        //此种不能随机，刚好全是最大值
        while (x > 0) {
          arr.push(max)
          x--
        }
      } else if (min * x == n) {
        //此种不能随机，刚好全是最小值
        while (x > 0) {
          arr.push(min)
          x--
        }
      } else {
        random = this.randomNum(min, max)
        if (n - random >= (x - 1) * min && n - random <= (x - 1) * max) {
          arr.push(random)
          n -= random
        } else {
          if (n - random < (x - 1) * min) {
            random = this.randomNum(min, n - (x - 1) * min)
          } else if (n - random > (x - 1) * max) {
            random = this.randomNum(n - (x - 1) * max, max)
          }
          arr.push(random)
          n -= random
        }
      }
      x--
    }

    return arr
  }
  //生成从minNum到maxNum的随机数
  randomNum(minNum, maxNum) {
    return parseInt(Math.random() * (maxNum - minNum + 1) + minNum)
  }

  /**
   * 初始化表盘数据，随机
   */
  private mytotal = 0
  private frontNum = 10
  @property(EditBox)
  frontNumEditBox
  initRandPanData() {
    this.frontNum = parseInt(this.frontNumEditBox.string || '0')
    this.mytotal = 0
    var fen = parseInt(this.total.string || '1')
    var sum = fen * 3
    if (sum < 3) sum = 3
    var cat = parseInt(this.cat.string || '0')
    if (cat < 3) cat = 3
    if (cat > 14) cat = 14

    if (fen < cat) {
      this.myAlert.open({
        title: '提示',
        content: '份数不能少于类数',
        confirmTxt: '好的',
      })
      return
    }
    //最少cat份
    if (fen < cat) {
      fen = cat
      this.total.string = fen + ''
    }
    this.panCoinsWrap.destroyAllChildren()
    this.nodeWrap.destroyAllChildren()

    //把total随机拆分
    var fens = this.intSplit(fen, cat)

    //随机抽出cat个类型
    resources.loadDir('coins', (err, d: Prefab[]) => {
      var arr = this.makeRandomArr(d, cat)
      var total = 0
      for (let index = 0; index < arr.length; index++) {
        const element = instantiate(arr[index])
        element.parent = this.panCoinsWrap
        element.setPosition(
          new Vec3(
            -7.255 + this.perCeil * (index >= 7 ? index - 7 : index),
            index >= 7 ? this.pery * 4 : 0,
            0,
          ),
        )
        element.getComponent(coin).static = true
        if (this.ifRandInit.isChecked) {
          element.getComponent(coin).panNum = 0
          var n = fens[index] * 3
          //随机
          for (let j = 0; j < n; j++) {
            //前30个，固定位置
            var total = this.mytotal
            if (total < this.frontNum * 2) {
              if (total < this.frontNum) {
                var x = 2 * this.perCeil
                var z = (this.panTop - 1) * this.perCeil
              } else {
                var x = -2 * this.perCeil
                var z = (this.panTop - 1) * this.perCeil
              }
              var i = total < this.frontNum ? total : total - this.frontNum
              var y = this.floory + i * this.pery
                ; ((x, y, z, i, name) => {
                  resources.load('coins/' + name, (err, p) => {
                    const element = instantiate(p as Prefab)
                    element.parent = this.nodeWrap
                      ; (element as any).zindex = i
                    // ;(element as any).zpian = z % this.perCeil != 0
                    // ;(element as any).xpian = x % this.perCeil != 0

                    element.setPosition(new Vec3(x, y, z))
                  })
                })(x, y, z, i, element.name)
              this.mytotal++
            } else {
              this.setRandCoin(element.name)
            }
          }
        } else element.getComponent(coin).panNum = fens[index] * 3
        element.getComponent(coin).staticPanNum = fens[index] * 3
      }
    })
  }
  /**
   * 在棋盘中随机放一个
   * @param name
   */
  setRandCoin(name, index = 0, x = null, z = null) {
    if (x == null)
      x =
        (this.randomNum(this.panLeft * 2 - 1, this.panRight * 2) *
          this.perCeil) /
        2
    if (z == null)
      z =
        (this.randomNum(this.panDown * 2 + 1, this.panTop * 2 - 6) *
          this.perCeil) /
        2

    var y = this.floory + index * this.pery

      ; ((x, y, z, index) => {
        resources.load('coins/' + name, (err, p) => {
          //如果有碰撞就上升一级
          if (this.checkIfPeng(new Vec3(x, y, z))) {
            //console.log(new Vec3(x, y, z))
            this.setRandCoin(name, index + 1, x, z)
            return
          }
          this.mytotal++
          const element = instantiate(p as Prefab)
          element.parent = this.nodeWrap
            ; (element as any).zindex = index
            ; (element as any).zpian = z % this.perCeil != 0
            ; (element as any).xpian = x % this.perCeil != 0

          element.setPosition(new Vec3(x, y, z))
        })
      })(x, y, z, index)
  }

  checkIfPeng(pos) {
    var array = this.nodeWrap.children
    for (let index = 0; index < array.length; index++) {
      const element = array[index]
      if (element.position.y == pos.y) {
        var v1 = new Vec2(pos.x, pos.z)
        var v2 = new Vec2(element.position.x, element.position.z)

        if (Vec2.len(v1.subtract(v2)) < this.perCeil) {
          //console.log(Vec2.len(v1.subtract(v2)))
          return true
        }
      }
    }
    return false
  }

  /**
   * 初始化表盘数据，随机
   */
  initPanDataByData(data) {
    //
    var panCoins = {}

    for (let index = 0; index < data.length; index++) {
      const element = data[index]
      if (!panCoins[element.name]) {
        panCoins[element.name] = 1
      } else {
        panCoins[element.name]++
      }
    }

    this.panCoinsWrap.destroyAllChildren()
    this.nodeWrap.destroyAllChildren()

    console.log(panCoins)

    var index = 0
    for (var n in panCoins) {
      const num = panCoins[n]
        ; ((num, index) => {
          resources.load('coins/' + n, (err, p) => {
            const element = instantiate(p as Prefab)

            element.setPosition(
              new Vec3(
                -7.255 + this.perCeil * (index >= 7 ? index - 7 : index),
                index >= 7 ? this.pery * 4 : 0,
                0,
              ),
            )
            //element.setPosition(new Vec3(-7.255 + this.perCeil * index, 0))
            element.getComponent(coin).static = true
            element.getComponent(coin).panNum = 0
            element.getComponent(coin).staticPanNum = num
            element.parent = this.panCoinsWrap
          })
        })(num, index)
      index++
    }
  }

  /**
   * 同步棋盘和托盘里数量
   */
  synStatus() {
    //console.log(this.scene)

    var coins = this.nodeWrap.children

    var data = {}
    for (let index = 0; index < coins.length; index++) {
      const element = coins[index]
      if (data[element.name]) {
        data[element.name].num++
      } else {
        data[element.name] = {
          num: 1,
        }
      }
    }

    var panCoins = this.panCoinsWrap.children
    //console.log(data, panCoins)
    //让动态数量=静态数量-现存数量
    for (let index = 0; index < panCoins.length; index++) {
      const element = panCoins[index]

      if (data[element.name])
        element.getComponent(coin).panNum =
          element.getComponent(coin).staticPanNum - data[element.name].num
      else
        element.getComponent(coin).panNum = element.getComponent(
          coin,
        ).staticPanNum
    }
  }

  move(node, add: Vec2) {
    var zindex = parseInt(this.zindex.progress * this.maxZindex + '')
    // console.log(this.zindex.progress);

    node.position = new Vec3(
      this.lastModifyCoin.position.x + add.x * this.perCeil,
      this.floory + this.pery * zindex,
      node.position.z + add.y * this.perCeil,
    )
    this.limitPos()
  }
  onKeyDown(event: EventKeyboard) {
    //console.log(event)
    //this.btnAu.play()
    this.au(this.btnAu, 1)
    if (this.lastModifyCoin != null && this.lastModifyCoin.position)
      switch (event.keyCode) {
        case 65: //左
          this.move(this.lastModifyCoin, new Vec2(-1, 0))
          break
        case 87: //上
          this.move(this.lastModifyCoin, new Vec2(0, -1))
          break
        case 68: //右边
          this.move(this.lastModifyCoin, new Vec2(1, 0))
          break
        case 83: //右边
          this.move(this.lastModifyCoin, new Vec2(0, 1))
          break
        case 37: //左
          this.move(this.lastModifyCoin, new Vec2(-1, 0))
          break
        case 38: //上
          this.move(this.lastModifyCoin, new Vec2(0, -1))
          break
        case 39: //右边
          this.move(this.lastModifyCoin, new Vec2(1, 0))
          break
        case 40: //右边
          this.move(this.lastModifyCoin, new Vec2(0, 1))
          break
        default:
          break
      }

    switch (event.keyCode) {
      case 89: //y
        this.zpian.isChecked = !this.zpian.isChecked
        break
      case 88: //y
        this.xpian.isChecked = !this.xpian.isChecked
        break
      //this.onXpian()
      case 81: //q
        this.moveZindex(-1)
        break
      case 69: //e
        this.moveZindex(1)
        break
      default:
        break
    }
  }
  private quessData: Array<Array<any>>
  private nowQuessIndex = 0
  private ad
  @property(Label)
  maxScoreLabel
  /**
   * 视频广告封装
   * @param options
   * @returns
   */
  gbShowVideoAd(options) {
    options.before = () => {
      this.node.getComponent(AudioSource).pause()
      this.loadingNode.active = true
    }
    options.after = () => {
      this.node.getComponent(AudioSource).play()
      this.loadingNode.active = false
    }
    options.loaded = () => { }
    var adid = this.wx_adid
    if (typeof tt != 'undefined') adid = this.tt_adid
    if (typeof qg != 'undefined') adid = this.vv_adid
    options.adid = adid
    return gb.ad(options)
  }
  private orgPos: Vec3

  isWeiXin = (): boolean => {
    const ua = window.navigator.userAgent.toLowerCase()

    const match = ua.match(/MicroMessenger/i)

    if (match === null) {
      return false
    }

    if (match.includes('micromessenger')) {
      return true
    }

    return false
  }
  ifHDEvent() {
    if (this.light) {
      ; (this.light as DirectionalLight).shadowEnabled = this.ifHD.isChecked
        ; (this
          .light as DirectionalLight).useColorTemperature = this.ifHD.isChecked
    }
    director.getScene().globals.skybox.useHDR = this.ifHD.isChecked
  }
  ifSlientEvent() {
    if (
      this.node.getComponent(AudioSource).playing &&
      this.ifSlient.isChecked
    ) {
      this.node.getComponent(AudioSource).pause()
    } else {
      this.node.getComponent(AudioSource).play()
    }
  }
  onLoad() {
    console.log('onload')
    this.fstLoadingNode.active = true
    if (this.wx_adid == '') {
      this.wxUseAd = false
    }

    // if (this.isRemoteVersion) {
    //   gb.r({
    //     name: 'getAppinfo',
    //     success(e) {
    //       console.log(e)
    //     },
    //   })
    // }
    if (this.rankBtn && this.isRemoteVersion) {
      this.rankBtn.active = true
    } else {
      if (this.rankBtn) this.rankBtn.active = false
    }
    //gb.r({})
    //ios网页中不支持SkeletalAnimation
    if (this.isWeiXin() && typeof wx == 'undefined') {
      if (this.character)
        this.character.getComponent(SkeletalAnimation).useBakedAnimation = true
    }

    this.orgPos = new Vec3(
      this.node.position.x,
      this.node.position.y,
      this.node.position.z,
    )
    this.nowQuessIndex = parseInt(
      sys.localStorage.getItem('nowQuessIndex') || '0',
    )
    this.score = parseInt(sys.localStorage.getItem('score') || '0')
    this.maxScoreLabel.string =
      '最高分' + (sys.localStorage.getItem('maxScore') || '0')
    //this.scene = 'make'
    this.scene = (game as any).scene || 'play'
    //构建棋盘
    //this.makePan()

    // if (typeof wx != 'undefined' && typeof tt == 'undefined') {
    //   wx.onShow(() => {
    //     if (wx.tmpAct) wx.tmpAct()
    //     wx.tmpAct = null
    //   })
    //   wx.showShareMenu({
    //     withShareTicket: true,
    //     menus: ['shareAppMessage', 'shareTimeline'],
    //   })
    //   /**
    //    * 右上角转发
    //    */
    //   wx.onShareAppMessage(() => {
    //     var title = this.shareTitle[0]
    //     if (this.shareTitleRand)
    //       title = this.shareTitle[this.randomNum(0, this.shareTitle.length - 1)]

    //     var url = this.shareImgUrlNormal[0]
    //     if (this.shareImgRand)
    //       url = this.shareImgUrlNormal[
    //         this.randomNum(0, this.shareImgUrlNormal.length - 1)
    //       ]

    //     console.log(title, url)

    //     return {
    //       title: title,
    //       imageUrl: url,
    //     }
    //   })
    //   /**
    //    * 分享到朋友圈
    //    */
    //   wx.onShareTimeline(() => {
    //     var title = this.shareTitle[0]
    //     if (this.shareTitleRand)
    //       title = this.shareTitle[this.randomNum(0, this.shareTitle.length - 1)]
    //     var url = this.shareImgUrlTimeline[0]
    //     if (this.shareImgRand)
    //       url = this.shareImgUrlTimeline[
    //         this.randomNum(0, this.shareImgUrlTimeline.length - 1)
    //       ]
    //     return {
    //       title: title,
    //       imageUrl: url,
    //     }
    //   })
    //   if (this.showBottomWxBanner) {
    //     this.toolBtnWrap.setPosition(new Vec3(0, 0, 0))
    //     this.panOutWrap.setPosition(new Vec3(0, 11.907, 0))
    //     // 创建 Banner 广告实例，提前初始化
    //     let bannerAd = wx.createBannerAd({
    //       adUnitId: this.wx_bannerid,
    //       style: {
    //         left: 0,
    //         top: wx.getSystemInfoSync().screenHeight - 100,
    //         width: wx.getSystemInfoSync().screenWidth
    //       }
    //     })

    //     // 在适合的场景显示 Banner 广告
    //     bannerAd.show()
    //     bannerAd.onError(() => { })

    //   }
    //   if (this.interstitialAdInter != -1) {
    //     var act = () => {

    //       tween(this.node).delay(this.interstitialAdInter).call(() => {
    //         // 定义插屏广告
    //         let interstitialAd = null

    //         // 创建插屏广告实例，提前初始化
    //         if (wx.createInterstitialAd) {
    //           interstitialAd = wx.createInterstitialAd({
    //             adUnitId: this.interstitialAdId
    //           })
    //         }

    //         // 在适合的场景显示插屏广告
    //         if (interstitialAd) {
    //           interstitialAd.show().catch((err) => {
    //             console.error(err)
    //           })
    //         }
    //         act()
    //       }).start()
    //     }

    //     act()
    //   }
    //   if (this.isRemoteVersion) {
    //     const button = wx.createUserInfoButton({
    //       type: 'text',
    //       text: '排行榜',
    //       style: {
    //         left: 10,
    //         top: 47,
    //         width: 80,
    //         height: 30,
    //         lineHeight: 30,
    //         backgroundColor: '#000000',
    //         color: '#ffffff',
    //         textAlign: 'center',
    //         fontSize: 16,
    //         borderRadius: 4
    //       }
    //     })
    //     if (!this.userData)
    //       button.onTap((res) => {
    //         this.userData = res
    //         // 此处可以获取到用户信息
    //         this.rank({}, res)
    //       })
    //     else {
    //       this.rank({}, this.userData)
    //     }
    //     this.rankBtn.active = false
    //   }


    // }
  }
  onFstLoadQuess() {
    if (typeof wx != 'undefined' && typeof tt == 'undefined') {
      wx.onShow(() => {
        if (wx.tmpAct) wx.tmpAct()
        wx.tmpAct = null
      })
      wx.showShareMenu({
        withShareTicket: true,
        menus: ['shareAppMessage', 'shareTimeline'],
      })
      /**
       * 右上角转发
       */
      wx.onShareAppMessage(() => {
        var title = this.shareTitle[0]
        if (this.shareTitleRand)
          title = this.shareTitle[this.randomNum(0, this.shareTitle.length - 1)]

        var url = this.shareImgUrlNormal[0]
        if (this.shareImgRand)
          url = this.shareImgUrlNormal[
            this.randomNum(0, this.shareImgUrlNormal.length - 1)
          ]

        console.log(title, url)

        return {
          title: title,
          imageUrl: url,
        }
      })
      /**
       * 分享到朋友圈
       */
      wx.onShareTimeline(() => {
        var title = this.shareTitle[0]
        if (this.shareTitleRand)
          title = this.shareTitle[this.randomNum(0, this.shareTitle.length - 1)]
        var url = this.shareImgUrlTimeline[0]
        if (this.shareImgRand)
          url = this.shareImgUrlTimeline[
            this.randomNum(0, this.shareImgUrlTimeline.length - 1)
          ]
        return {
          title: title,
          imageUrl: url,
        }
      })
      if (this.showBottomWxBanner) {
        this.toolBtnWrap.setPosition(new Vec3(0, 0, 0))
        this.panOutWrap.setPosition(new Vec3(0, 11.907, 0))
        // 创建 Banner 广告实例，提前初始化
        let bannerAd = wx.createBannerAd({
          adUnitId: this.wx_bannerid,
          style: {
            left: 0,
            top: wx.getSystemInfoSync().screenHeight - 100,
            width: wx.getSystemInfoSync().screenWidth
          }
        })

        // 在适合的场景显示 Banner 广告
        bannerAd.show()
        bannerAd.onError(() => { })

      }
      if (this.interstitialAdInter != -1) {
        var act = () => {

          tween(this.node).delay(this.interstitialAdInter).call(() => {
            // 定义插屏广告
            let interstitialAd = null

            // 创建插屏广告实例，提前初始化
            if (wx.createInterstitialAd) {
              interstitialAd = wx.createInterstitialAd({
                adUnitId: this.interstitialAdId
              })
            }

            // 在适合的场景显示插屏广告
            if (interstitialAd) {
              interstitialAd.show().catch((err) => {
                console.error(err)
              })
            }
            act()
          }).start()
        }

        act()
      }
      if (this.isRemoteVersion) {
        const button = wx.createUserInfoButton({
          type: 'text',
          text: '排行榜',
          style: {
            left: 10,
            top: 47,
            width: 80,
            height: 30,
            lineHeight: 30,
            backgroundColor: '#000000',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: 16,
            borderRadius: 4
          }
        })
        if (!this.userData)
          button.onTap((res) => {
            this.userData = res
            // 此处可以获取到用户信息
            this.rank({}, res)
          })
        else {
          this.rank({}, this.userData)
        }
        this.rankBtn.active = false
      }


    }
  }
  private userData
  init() {
    if (this.scene == 'make') {
      this.deskBlank.active = true
      this.deskBlank.getComponent(BoxCollider).enabled = true
      this.targetDesk.getComponent(BoxCollider).enabled = true
      this.UIMake.active = true
      this.UIPlay.active = false
      input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this)
      input.on(Input.EventType.KEY_PRESSING, this.onKeyDown, this)
      this.getQuessBtn.on(
        Input.EventType.TOUCH_START,
        () => {
          this.getQuessData()
        },
        this,
      )
      // this.initBtn.on(
      //   Input.EventType.TOUCH_START,
      //   () => {
      //     this.initRandPanData()
      //   },
      //   this,
      // )
      this.deskBlank.on('DIYTOUCHEND', () => {
        //移动中的才能删除
        if (!this.moving) return
        console.log(this.tmp)
        if (this.tmp) this.tmp.destroy()
        this.tmp = null
        this.lastModifyCoin = null
      })
      //this.initPanData();
      this.deleteArea.on('DIYTOUCHEND', () => {
        if (!this.moving) return
        console.log(this.tmp)
        if (this.tmp) this.tmp.destroy()
        this.tmp = null
        this.lastModifyCoin = null
        this.deleteArea.position = this.deleteArea.position.add(
          new Vec3(0, 0.1, 0),
        )
      })
      this.deleteArea.on('DIYTOUCHSTART', () => {
        this.lastModifyCoin = null
        this.tmp = null
        this.deleteArea.position = this.deleteArea.position.add(
          new Vec3(0, -0.1, 0),
        )
      })
      this.scoreLabel.node.active = false

      this.getDataRermote((data) => {
        this.quessData = data
        if (typeof this.nowQuessIndex != 'undefined') {
          this.makeMakeQuess()
        }
      })
    } else if (this.scene == 'play') {
      this.deskBlank.active = false
      this.targetDesk.getComponent(BoxCollider).enabled = false
      this.lastModifyCoin = null
      this.tmp = null
      this.holderHover.active = false
      this.holderBox.active = false
      this.UIPlay.active = true
      this.UIMake.active = false
      this.deleteArea.active = false
      this.getDataRermote((data) => {
        this.quessData = data
        this.reloadQuess()
      })
    }
  }
  start() {
    this.ifHDEvent()
    this.ifSlientEvent()
    //tween(this.node).delay(0.5).to(2, { position: new Vec3(0, 67.295, 3.033), eulerAngles: new Vec3(-90, 0, 0) }).start();
    input.on(Input.EventType.TOUCH_MOVE, this.onTouchMove, this)
    //input.on(Input.EventType.TOUCH_MOVE, this.onTouchMove2, this);
    input.on(Input.EventType.TOUCH_START, this.onTouchStart, this)
    input.on(Input.EventType.TOUCH_END, this.onTouchEnd, this)
    input.on(Input.EventType.TOUCH_CANCEL, this.onTouchCancel, this)
    input.on(Input.EventType.MOUSE_WHEEL, this.onMouseWheel, this)
    this.init()
  }

  /**
   * 加载关卡
   */
  @property(Node)
  loadingNode
  @property(Node)
  fstLoadingNode

  private hasInitLoad = false
  reloadQuess(n = 0, fromLost = false) {
    var loading = this.loadingNode
    if (!this.hasInitLoad) {
      loading = this.fstLoadingNode
      this.hasInitLoad = true
    }

    var self = this
    if (n == 1) {
      loading.active = true
      this.nowQuessIndex++

      tween(this.node)
        .delay(0.5)
        .call(() => {
          if (
            !this.makeQuess(true, () => {
              tween(this.node)
                .delay(1)
                .call(() => {
                  loading.active = false
                  if (loading == this.fstLoadingNode) {
                    this.onFstLoadQuess()
                  }
                  //BatchingUtility.batchStaticModel(this.nodeWrap, this.nodeWrap)
                })
                .start()
              if (typeof tt != 'undefined')
                // this.node.getComponent(ttRecord).recordStart()
                console.log("2050")
            })
          ) {
            this.nowQuessIndex--
            console.log('没有下一关了')
          } else {
            this.pullLimit = 1
            this.aliveLimit = 2
            this.randLimit = 1
            this.redoLimit = 1
          }
        })
        .start()
    } else {
      loading.active = true
      tween(this.node)
        .delay(1)
        .call(() => {
          this.makeQuess(true, () => {
            tween(this.node)
              .delay(1)
              .call(() => {
                loading.active = false
                if (loading == this.fstLoadingNode) {
                  this.onFstLoadQuess()
                }
                //BatchingUtility.batchStaticModel(this.nodeWrap, this.nodeWrap)
              })
              .start()
            if (typeof tt != 'undefined')
              // this.node.getComponent(ttRecord).recordStart()
              console.log("2081")
          })
        })
        .start()
      this.aliveLimit = 2
      this.pullLimit = 1
      this.randLimit = 1
      this.redoLimit = 1
    }

    sys.localStorage.setItem('nowQuessIndex', this.nowQuessIndex + '')
    sys.localStorage.setItem('score', this.score + '')
  }

  nextPlayQuess() {
    //this.btnAu.play()
    this.au(this.btnAu, 1)
    this.nowQuessIndex++
    if (!this.makeQuess()) {
      this.nowQuessIndex--
      alert('没有下一关了')
    }
  }
  prevPlayQuess() {
    //this.btnAu.play()
    this.au(this.btnAu, 1)
    this.nowQuessIndex--
    if (!this.makeQuess()) {
      this.nowQuessIndex++
      alert('没有上一关了')
    }
  }
  nextMakeQuess() {
    var self = this
    this.myAlert.open({
      content: '放弃保存进入下一关吗？',
      needCancel: true,
      confirm() {
        //this.btnAu.play()
        self.au(self.btnAu, 1)
        self.nowQuessIndex++
        if (!self.makeMakeQuess()) {
          self.quessData[self.nowQuessIndex] = []
          self.makeMakeQuess()
        }
      },
    })
  }
  prevMakeQuess() {
    var self = this
    this.myAlert.open({
      content: '放弃保存进入上一关吗？',
      needCancel: true,
      confirm() {
        //this.btnAu.play()
        self.au(self.btnAu, 1)
        self.nowQuessIndex--
        if (!self.makeMakeQuess()) {
          self.nowQuessIndex++
          //alert('sdfsd')
          self.myAlert.open({
            content: '没有上一关了',
          })
        }
      },
    })
  }
  makePan() {
    for (let x = this.panLeft; x <= this.panRight; x++) {
      for (let z = this.panDown - 1; z <= this.panTop; z++) {
        var n = instantiate(this.holderCeil)
        n.parent = this.holderCeil.parent
        n.active = true
        n.setPosition(new Vec3(x * this.perCeil, this.floory, z * this.perCeil))
      }
    }
  }

  checkPlayStatus() { }

  update(deltaTime: number) {
    if (this.scene == 'make') {
      // var grx = -90;
      // var gy = 67.295;
      // var gz = 3.033;
      // if(this.node.eulerAngles.x>grx)
      //     this.node.eulerAngles = new Vec3(this.node.eulerAngles.x - 2, 0, 0);
      // if (this.node.position.y < gy)
      //     this.node.setPosition(new Vec3(this.node.position.x, this.node.position.y + 2, this.node.position.z))
      // if (this.node.position.z > gz)
      //     this.node.setPosition(new Vec3(this.node.position.x, this.node.position.y, this.node.position.z-2))
      // console.log(this.node.eulerAngles.x, this.node.position)
      if (this.lastModifyCoin && this.lastModifyCoin.position) {
        this.holderHover.active = true

        this.holderHover.position = new Vec3(
          this.lastModifyCoin.position.x,
          this.floory,
          this.lastModifyCoin.position.z,
        )
        this.holderBox.active = true
        //console.log(this.tmp.position.y);
        this.holderBox.position = this.lastModifyCoin.position
      } else {
        this.holderHover.active = false
        this.holderBox.active = false
      }
      //

      this.synStatus()
    }
    if (this.scene == 'play') {
      for (let index = 0; index < 3; index++) {
        if (this.activeQuee.length > 0) {
          var n = this.activeQuee.pop()
          n.active = true
          n.getComponent(coin).coinstart()
        }
      }
    }
  }
  addCoin(event: EventTouch = null, cb = null) {
    //获取当前点中金币
    this.cameraCom.screenPointToRay(
      event.getLocationX(),
      event.getLocationY(),
      this._ray2,
    )
    //console.log(touch.getLocation());

    if (PhysicsSystem.instance.raycast(this._ray2)) {
      const raycastResults = PhysicsSystem.instance.raycastResults

      for (let i = 0; i < raycastResults.length; i++) {
        const item = raycastResults[i]
        // console.log(
        //   item.collider.node.name,
        //   raycastResults,
        //   raycastResults.length,
        // )

        if (
          item.collider.node.name == 'desk' ||
          (item.collider.node.getComponent(coin) &&
            item.collider.node.getComponent(coin).isFly) ||
          (item.collider.node.getComponent(coin) &&
            item.collider.node.getComponent(coin).isUnder &&
            this.underNoMove.isChecked)
        ) {
          // 被盖住的不能动
          // this.tmp = null;
          //this.btnAu.play()
          this.au(this.btnAu, 1)
          continue
        } else if (item.collider.node.parent == this.nodeWrap) {
          //this.btnAu.play()
          this.au(this.btnAu, 1)
          //移动棋盘上的
          // if (this.tmp)
          //     this.tmp.getChildByName('select').active = false;
          for (let index = 0; index < this.nodeWrap.children.length; index++) {
            const element = this.nodeWrap.children[index]
            element.getChildByName('select').active = false
          }
          this.tmp = item.collider.node
          //this.tmp.getChildByName('select').active = true;

          this.zindex.progress = (this.tmp as any).zindex / this.maxZindex
          find('Handle/Label', this.zindex.node).getComponent(Label).string =
            (this.tmp as any).zindex + 1 + ''
          this.lastModifyCoin = null //需要
          this.xpian.isChecked = (this.tmp as any).xpian

          this.zpian.isChecked = (this.tmp as any).zpian

          this.lastModifyCoin = this.tmp

          break
        } else {
          if (item.collider.node.getComponent(coin))
            resources.load('coins/' + item.collider.node.name, (err, data) => {
              if (err) {
                //console.log(err);
                // if (cb)
                //     cb()
                return
              }
              //console.log(data)
              var n = instantiate(data as Prefab)

              var zindex = parseInt(this.zindex.progress * this.maxZindex + '')
              //偏移到附近的整点坐标
              var y = this.floory + this.pery * zindex
              //console.log(item.collider.node.getSiblingIndex());

              // n.position = new Vec3(
              //   (item.collider.node.getSiblingIndex() - 3) * this.perCeil,
              //   y,
              //   5 * this.perCeil,
              // )
              n.parent = this.nodeWrap

              for (
                let index = 0;
                index < this.nodeWrap.children.length;
                index++
              ) {
                const element = this.nodeWrap.children[index]
                element.getChildByName('select').active = false
              }
              if (this.tmp) this.tmp.getChildByName('select').active = false
              this.tmp = n
                ; (this.tmp as any).xpian = this.xpian.isChecked
                ; (this.tmp as any).zpian = this.zpian.isChecked
                ; (this.tmp as any).zindex = zindex

              if (cb) cb()
            })
        }
      }
    }
  }
  setPos(event: EventTouch) {
    if (!this.tmp) return
    //const touch = event!;
    this.cameraCom.screenPointToRay(
      event.getLocationX(),
      event.getLocationY(),
      this._ray,
    )
    //console.log(touch.getLocation());

    if (PhysicsSystem.instance.raycast(this._ray)) {
      const raycastResults = PhysicsSystem.instance.raycastResults
      //console.log(raycastResults)

      for (let i = 0; i < raycastResults.length; i++) {
        const item = raycastResults[i]
        //console.log(item.collider.node.name)

        if (item.collider.node == this.targetDesk) {
          //console.log(item.hitPoint);
          //this.endPoint = item.hitPoint;

          // var x = Math.ceil(this.endPoint.x)
          // var z = Math.ceil(this.endPoint.z)
          // this.tmp.position = item.hitPoint;
          // if ((x * 100) % 250 == 0 && (z * 100) % 250 == 0)

          if (this.tmp) {
            this.tmp.parent = this.nodeWrap
            this.lastModifyCoin = this.tmp
          }

          var zindex = parseInt(this.zindex.progress * this.maxZindex + '')
          // console.log(this.zindex.progress);

          this.tmp.position = new Vec3(
            item.hitPoint.x,
            this.floory + this.pery * zindex,
            item.hitPoint.z,
          )
          // console.log(this.tmp.position, item.collider.node.name);
          //偏移到附近的整点坐标
          var y = this.floory + this.pery * zindex
          var n = this.tmp.position
          var g = new Vec2(n.x, n.z).rotate(
            (this.desk.eulerAngles.y * Math.PI) / 180,
          )

          //this.tmp.position = new Vec3(g.x, y, g.y)

          var per = this.perCeil
          // var newx =
          //   Math.floor(this.tmp.position.x / per) * per -
          //   per -
          //   ((this.xpian.isChecked ? 1 : 0) * per) / 2
          // var newz =
          //   Math.floor(this.tmp.position.z / per) * per -
          //   ((this.zpian.isChecked ? 1 : 0) * per) / 2
          var pos = this.findFallPoint(this.tmp, new Vec2(g.x, g.y))
          var newx = pos.x
          var newz = pos.y

          this.tmp.position = new Vec3(newx, y, newz)

          // var g=(new Vec2(newx, newz)).rotate(this.desk.eulerAngles.y * Math.PI / 180)

          // this.tmp.position = (new Vec3(g.x, y, g.y));

          this.limitPos()
          //console.log(this.tmp.position);

          //重叠,升index
          // var as = this.getCoinsByXZ(this.tmp.position.x, this.tmp.position.z);
          // if (as[as.length - 2])
          //     this.tmp.position = new Vec3(this.tmp.position.x, as[as.length - 2].position.y + this.pery, this.tmp.position.z)
          break
        } else if (item.collider.node == this.deskBlank) {
          if (this.tmp) {
            this.tmp.worldPosition = item.hitPoint
          }
          this.lastModifyCoin = null
          break
        }
      }
    } else {
      //console.log('raycast does not hit the target node !', event);
      //this.desk.eulerAngles=
    }
  }
  onSlider() {
    var i = parseInt(this.zindex.progress * this.maxZindex + '')
    this.zindex.progress = i / this.maxZindex

    find('Handle/Label', this.zindex.node).getComponent(Label).string =
      i + 1 + ''

    //
    if (this.lastModifyCoin != null && this.lastModifyCoin.position) {
      var zindex = parseInt(this.zindex.progress * this.maxZindex + '')
      // console.log(this.zindex.progress);
      var y = this.floory + this.pery * zindex
      //console.log(this.lastModifyCoin);

      this.lastModifyCoin.position = new Vec3(
        this.lastModifyCoin.position.x,
        y,
        this.lastModifyCoin.position.z,
      )
        ; (this.lastModifyCoin as any).zindex = zindex
    }
  }
  onXpian() {
    console.log(this.lastModifyCoin)

    if (this.lastModifyCoin != null && this.lastModifyCoin.position) {
      if ((this.lastModifyCoin as any).xpian) {
        //如果偏移过
        this.lastModifyCoin.position = this.lastModifyCoin.position.add(
          new Vec3(this.perCeil / 2, 0, 0),
        )
      } else {
        this.lastModifyCoin.position = this.lastModifyCoin.position.add(
          new Vec3(-this.perCeil / 2, 0, 0),
        )
      }

      ; (this.lastModifyCoin as any).xpian = this.xpian.isChecked
      this.limitPos()
    }
  }
  onZpian() {
    if (this.lastModifyCoin != null && this.lastModifyCoin.position) {
      if ((this.lastModifyCoin as any).zpian) {
        //如果偏移过
        this.lastModifyCoin.position = this.lastModifyCoin.position.add(
          new Vec3(0, 0, this.perCeil / 2),
        )
      } else {
        this.lastModifyCoin.position = this.lastModifyCoin.position.add(
          new Vec3(0, 0, -this.perCeil / 2),
        )
      }

      ; (this.lastModifyCoin as any).zpian = this.zpian.isChecked
      this.limitPos()
    }
  }

  findFallPoint(node, pos) {
    var nx = node.position.x
    for (let x = this.panLeft; x <= this.panRight; x++) {
      if ((node as any).xpian) {
        if (
          pos.x < x * this.perCeil &&
          pos.x >= x * this.perCeil - this.perCeil
        ) {
          nx = x * this.perCeil - this.perCeil / 2
          console.log(nx)

          break
        }
      } else {
        if (
          pos.x < x * this.perCeil + this.perCeil / 2 &&
          pos.x >= x * this.perCeil - this.perCeil / 2
        ) {
          nx = x * this.perCeil
          break
        }
      }
    }
    var ny = node.position.y
    for (let y = this.panDown; y <= this.panTop; y++) {
      if ((node as any).zpian) {
        if (
          pos.y < y * this.perCeil &&
          pos.y >= y * this.perCeil - this.perCeil
        ) {
          ny = y * this.perCeil - this.perCeil / 2
          break
        }
      } else {
        if (
          pos.y < y * this.perCeil + this.perCeil / 2 &&
          pos.y >= y * this.perCeil - this.perCeil / 2
        ) {
          ny = y * this.perCeil
          break
        }
      }
    }
    //console.log(nx)

    return new Vec2(nx, ny)
  }
  onTouchStart(event: EventTouch) {
    //给点击物体emit事件
    this.regEvent('DIYTOUCHSTART', event)
    this.regEvent('DIYTOUCHSTART-THROUGH', event, true)
    if (this.scene == 'make') {
      //if (event.getButton() == 2) {
      this.addCoin(event, () => {
        //this.setPos(event);
      })

      //}
    }
  }
  onMouseWheel(ev: EventMouse) {
    if (this.scene == 'make') {
      //parseInt((ev.getScrollY()) / 40 + '')
      if (Math.abs(ev.getScrollY()) < 100) {
        return
      }
      this.moveZindex(ev.getScrollY())
    }
  }
  moveZindex(addPos) {
    console.log(addPos)

    var add = 1
    if (addPos < 0) {
      add = -1
    }
    var i = parseInt(this.zindex.progress * this.maxZindex + '') + add
    if (i < 0) i = 0
    if (i > this.maxZindex) i = this.maxZindex
    this.zindex.progress = i / this.maxZindex
    find('Handle/Label', this.zindex.node).getComponent(Label).string =
      i + 1 + ''
    this.onSlider()
  }
  onTouchMove(event: EventTouch) {
    this.moving = true
    this.regEvent('DIYTOUCHMOVE', event)
    this.regEvent('DIYTOUCHMOVE-THROUGH', event, true)
    if (this.scene == 'make') {
      //console.log(event.getLocationX());

      //console.log('mouse', event.getLocationX(), event.getLocationY());

      if (!this.moving) return

      // if (this.tmp)
      //     this.tmp.getChildByName('select').active = true;
      this.setPos(event)

      // console.log(
      //     event.getDeltaX(),
      //     Vec2.len((event.touch._startPoint as Vec2).subtract(event.touch._point))
      // );
    }
    if (!this.tmp) {
      //tween(this.desk).to(0.3, { eulerAngles: this.desk.eulerAngles.add(new Vec3(0, event.getDeltaX(), 0)) })
      this.desk.eulerAngles = this.desk.eulerAngles.add(
        new Vec3(0, event.getDeltaX() / 20, 0),
      )
    }
  }
  // onTouchMove2(event: EventTouch) {
  //     console.log('touch', event.touch.getLocationX(), event.touch.getLocationY());
  // }
  onTouchEnd(event: EventTouch) {
    this.regEvent('DIYTOUCHEND', event)
    this.regEvent('DIYTOUCHEND-THROUGH', event, true)
    if (this.scene == 'make') {
      // var y = this.floory + this.pery * (parseInt(this.zindex.string || "0"));
      // var n = this.tmp.position;
      // var per = 2.5;
      // var newx = (parseInt(n.x / per + '')) * per;
      // var newz = (parseInt(n.z / per + '')) * per;
      // this.tmp.position = new Vec3(newx, y, newz);
      //如果没有移动

      this.moving = false

      if (this.tmp) {
        //重叠,升index
        var zindex = parseInt(this.zindex.progress * this.maxZindex + '')
        var as = this.getCoinsByPos(this.tmp.position)
        if (as.samey.length == 1) {
          //空位置
        } else if (as.samey.length == 2) {
          //重复了
          //alert('当前位置有筹码了');
          as.samey[0].destroy()
          // this.tmp.destroy();
          // this.tmp = null;
        } else if (as.all.length > 1) {
          zindex = as.all.length - 1
          this.zindex.progress = zindex / this.maxZindex
          find('Handle/Label', this.zindex.node).getComponent(Label).string =
            zindex + 1 + ''

          this.tmp.position = new Vec3(
            this.tmp.position.x,
            as.all[as.all.length - 2].position.y + this.pery,
            this.tmp.position.z,
          )
        }
        if (this.tmp) {
          this.limitPos()
            //if(x<)
            ; (this.tmp as any).zindex = zindex
            ; (this.tmp as any).xpian = this.xpian.isChecked
            ; (this.tmp as any).zpian = this.zpian.isChecked

          //this.tmp.getChildByName('select').active = false;
          this.tmp = null
        }
      }
    }
  }
  limitPos() {
    var n = this.tmp || this.lastModifyCoin
    if (n != null) {
      //约束区域
      var x = n.position.x,
        y = n.position.y,
        z = n.position.z

      if (!(n as any).xpian) {
        //console.log(x, y, z);
        if (x < this.perCeil * this.panLeft) {
          x = this.perCeil * this.panLeft
        }
        if (x > this.perCeil * this.panRight) {
          x = this.perCeil * this.panRight
        }
      } else {
        if (x < this.perCeil * this.panLeft + this.perCeil / 2) {
          x = this.perCeil * this.panLeft + this.perCeil / 2
        }
        if (x > this.perCeil * this.panRight - this.perCeil / 2) {
          x > this.perCeil * this.panRight - this.perCeil / 2
        }
      }
      if (!(n as any).zpian) {
        if (z < this.perCeil * this.panDown) {
          z = this.perCeil * this.panDown
        }
        if (z > this.perCeil * this.panTop) {
          z = this.perCeil * this.panTop
        }
      } else {
        if (z < this.perCeil * this.panDown + this.perCeil / 2) {
          z = this.perCeil * this.panDown + this.perCeil / 2
        }
        if (z > this.perCeil * this.panTop - this.perCeil / 2) {
          z = this.perCeil * this.panTop - this.perCeil / 2
        }
      }

      if (y < this.floory + 0 * this.pery) {
        y = this.floory
      }
      if (y > this.floory + this.maxZindex * this.pery) {
        y = this.floory + this.maxZindex * this.pery
      }

      n.position = new Vec3(x, y, z)
    }
  }
  onTouchCancel(event: EventTouch) {
    this.regEvent('DIYTOUCHCANCEL', event)
    this.regEvent('DIYTOUCHCANCEL-THROUGH', event, true)
  }
  regEvent(name: string, event, ifChuantou = false) {
    //给点击物体emit事件
    this.cameraCom.screenPointToRay(
      event.getLocationX(),
      event.getLocationY(),
      this._ray,
    )
    if (PhysicsSystem.instance.raycast(this._ray)) {
      const raycastResults = PhysicsSystem.instance.raycastResults
      //console.log(raycastResults);

      for (let i = 0; i < raycastResults.length; i++) {
        const item = raycastResults[i]
        item.collider.node.emit(name)
        //if (!ifChuantou) break
      }
    }
  }
  getCoinsByPos(pos) {
    var cs = this.nodeWrap.children
    var as = []
    var asy = []
    for (let index = 0; index < cs.length; index++) {
      const element = cs[index]
      if (element.position.x == pos.x && element.position.z == pos.z) {
        as.push(element)
      }
      if (
        element.position.x == pos.x &&
        element.position.z == pos.z &&
        element.position.y == pos.y
      ) {
        asy.push(element)
      }
    }
    return { all: as, samey: asy }
  }
  /**
   * 打开排行榜，需要网络支持
   */
  rank(e, res) {
    if (!this.isRemoteVersion) return
    if (this.userData) {
      this.rankPop.show()
    } else
      gb.updateUserFace((e) => {
        console.log(e)
        this.rankPop.show()
      }, res)
  }
}
