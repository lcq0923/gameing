import {
  _decorator,
  Component,
  Node,
  Input,
  MeshRenderer,
  Vec3,
  geometry,
  PhysicsSystem,
  UITransform,
  Color,
  find,
  Label,
  Camera,
  Layers,
  Vec2,
  CylinderCollider,
} from 'cc'
import { camera } from './camera'
const { ccclass, property } = _decorator

/**
 * Predefined variables
 * Name = NewComponent_001
 * DateTime = Fri Sep 16 2022 18:55:55 GMT+0800 (中国标准时间)
 * Author = minijoe
 * FileBasename = NewComponent-001.ts
 * FileBasenameNoExtension = NewComponent-001
 * URL = db://assets/sc/NewComponent-001.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */

@ccclass('coin')
export class coin extends Component {
  // [1]
  // dummy = '';

  // [2]
  @property
  score = 0
  @property
  static = false

  @property
  _isUnder = false

  private newColor = new Color()
  private orgColor = new Color()

  @property //金币get/set方法
  public get isUnder() {
    return this._isUnder
  }

  public set isUnder(value) {

    this._isUnder = value

    // var pass = this.node.getComponent(MeshRenderer).getMaterialInstance(0)
    //   .passes[0]
    // var pass2 = find('coin.001', this.node)
    //   .getComponent(MeshRenderer)
    //   .getMaterialInstance(0).passes[0]
    // var gray = new Color(50, 50, 50)
    // var light = new Color(255, 255, 255)
    // if (value) {
    //   pass.setUniform(pass.getHandle('albedo'), gray)
    //   pass2.setUniform(pass2.getHandle('albedo'), gray)
    // } else {
    //   pass.setUniform(pass.getHandle('albedo'), light)
    //   pass2.setUniform(pass2.getHandle('albedo'), light)
    // }

    if (value) {
      find('coin.001', this.node).active = false
      find('coin.002', this.node).active = true
    } else {
      find('coin.002', this.node).active = false
      find('coin.001', this.node).active = true
    }
  }
  private panNumLabel: Label
  private panNumNode: Node

  /**
   * 缓存数量
   */
  @property
  _panNum = 0
  @property //金币get/set方法
  public get panNum() {
    return this._panNum
  }
  public set panNum(value) {
    this._panNum = value
    if (this.panNumLabel)
      this.panNumLabel.string = '' + value + '/' + this.staticPanNum
  }

  /**
   * 静态数量
   */
  @property //金币get/set方法
  _staticPanNum = 0

  private staticPanNumLabel: Label
  private staticPanNumNode: Node

  @property //金币get/set方法
  public get staticPanNum() {
    return this._staticPanNum
  }
  public set staticPanNum(value) {
    this._staticPanNum = value
    //this.staticPanNumLabel.string = '/' + value
  }

  @property
  isFly = false
  adjustColor(color, range) {
    let newColor = '#'
    for (let i = 0; i < 3; i++) {
      const hxStr = color.substr(i * 2 + 1, 2)
      let val = parseInt(hxStr, 16)
      val += range
      if (val < 0) val = 0
      else if (val > 255) val = 255
      newColor += val.toString(16).padStart(2, '0')
    }
    return newColor
  }

  private cam
  onLoad() {
    //find('coin.001', this.node).active = false
    //find('dir', this.node).active = false
    //console.log(Input.EventType.TOUCH_START)
    //this.node.getComponent(CylinderCollider).enabled = false
    this.cam = find('Main Camera').getComponent(camera)
    if (typeof wx == 'undefined' || typeof tt == 'undefined') {
    } else {
      this.node.getComponent(MeshRenderer).shadowCastingMode = 1
      this.node.getComponent(MeshRenderer).receiveShadow = 1
    }
    this.node.on(
      'DIYTOUCHSTART',
      () => {
        //console.log('coin touch');
      },
      this,
    )

    this.node.on(
      'DIYTOUCHMOVE',
      () => {
        //console.log('coin move');
      },
      this,
    )

    // var pass = this.node.getComponent(MeshRenderer).getMaterialInstance(0).passes[0];
    // var out = new Color();
    // pass.getUniform(pass.getHandle('emissive'), out);
    // // var h = new Color();
    // // Color.fromHEX(h,out.toHEX())
    // // console.log(out.toHEX(), h);

    // this.orgColor = out;

    // Color.fromHEX(this.newColor, this.adjustColor('#' + out.toHEX(), -100));

    // console.log(this.orgColor, this.newColor);

    //console.log((this.node.getComponent(MeshRenderer).material as any)._passes[0]._properties.emissiveScale.value = [0.5, 1, 1]);
    if (this.static) {
      this.createPanNumNode()
    }

    // this.staticPanNumNode = new Node()
    // this.staticPanNumNode.addComponent(UITransform)
    // this.staticPanNumLabel = this.staticPanNumNode.addComponent(Label)
    // this.staticPanNumLabel.string = '0'
    // this.staticPanNumLabel.color = new Color(255, 255, 0)
    // this.staticPanNumLabel.fontSize = 20
    // this.staticPanNumLabel.node.parent = find('Canvas')
    // this.staticPanNumNode.layer = Layers.Enum.UI_2D
  }
  createPanNumNode() {
    this.panNumNode = new Node()
    this.panNumNode.addComponent(UITransform)
    this.panNumLabel = this.panNumNode.addComponent(Label)
    this.panNumLabel.string = '0'
    this.panNumLabel.color = new Color(255, 255, 255)
    this.panNumLabel.fontSize = 15
    this.panNumLabel.node.parent = find('Canvas')
    this.panNumNode.layer = Layers.Enum.UI_2D
  }
  start() {
    if (this.static) {
      if (this.panNumNode)
        this.panNumNode.active = true
      //this.staticPanNumNode.active = true
    } else {
      if (this.panNumNode)
        this.panNumNode.active = false
      //this.staticPanNumNode.active = false
    }
  }
  coinstart() {
    this.node.emit('COINSTART')
  }
  onDestroy() {
    if (this.panNumNode)
      this.panNumNode.destroy()
    //this.staticPanNumNode.destroy()
  }
  private dirs = []
  private time = 0
  updateByPer(per = 60, cb) {
    //每隔一秒检查
    if (this.time > per) {
      this.time = 1
    }
    if (this.time % per == 0) {
      if (cb) cb()
    }
    this.time++
  }
  update(deltaTime: number) {
    if (this.cam.scene == 'make') {
      if (!this.static) {
        //如果
        // this.updateByPer(60, () => {
        //   this.isUnder = this.checkIfUnder()
        // })
        //如果是play模式
        if (this.cam.scene == 'play') {
          this.updateByPer(30, () => {
            this.isUnder = this.checkIfUnder()
          })
        } else {
          this.isUnder = this.checkIfUnder()
        }
        //console.log(this.time)
      } else {
        if (!this.panNumNode) {
          this.createPanNumNode()
        }
        if (this.panNum <= 0) {
          this.isUnder = true
        } else {
          this.isUnder = false
        }
      }
      if (this.static) {
        let _v3_0: Vec3 = new Vec3(0, 0, 0)

        this.node.getWorldPosition(_v3_0)

        let _v3_1: Vec3 = new Vec3(0, 0, 0)

        this.node.getWorldPosition(_v3_1)

        //console.log("p3:" + _v3_0)
        if (this.panNumLabel) {
          find('Main Camera')
            .getComponent(Camera)
            .convertToUINode(_v3_0, this.panNumLabel.node.parent, _v3_0)

          // find('Main Camera')
          //   .getComponent(Camera)
          //   .convertToUINode(_v3_1, this.panNumLabel.node.parent, _v3_1)

          //console.log("p2:" + _v3_0)

          this.panNumLabel.node.setPosition(_v3_0.add(new Vec3(-0, -20, 0)))
          //this.staticPanNumLabel.node.setPosition(_v3_1.add(new Vec3(-20, 10, 0)))
        }
      }
    }
  }

  checkIfUnder() {
    return this.checkIfUnderByPos()
  }
  //性能好像差一点点
  checkIfUnderByPhy() {
    var bb = false
    var num = 8
    for (let index = 0; index < num; index++) {
      var point = find('dir/topUp', this.node)
      var p = new Vec2(0, -0.9)
      p.rotate((index * (360 / num) * Math.PI) / 180)
      point.setPosition(p.x, -0.1, p.y)
      var p1 = new Vec3()
      point.getWorldPosition(p1)
      point.setPosition(p.x, 0.1, p.y)
      var p2 = new Vec3()
      point.getWorldPosition(p2)

      var outRay = new geometry.Ray()
      geometry.Ray.fromPoints(outRay, p1, p2)

      if (PhysicsSystem.instance.raycast(outRay)) {
        var raycastResults = PhysicsSystem.instance.raycastResults

        var b = false
        for (let i = 0; i < raycastResults.length; i++) {
          const item = raycastResults[i]
          //console.log(this.node.getComponent('coin'));
          if (
            item.collider.node.getComponent(coin) != null &&
            item.collider.node != this.node &&
            !item.collider.node.getComponent(coin).isFly
          ) {
            //console.log(raycastResults);
            //console.log('发生覆盖', this.isUnder, this.node.name, item.collider.node.name);
            b = true
            if (!this.isUnder) this.isUnder = true
            break
          }
        }
        if (b) {
          bb = true
          break
        }
      }
    }
    // if (!bb) {
    //   if (this.isUnder) this.isUnder = false
    // }
    return bb
  }
  checkIfUnderByPos() {
    //加一或n层的点发生碰撞,那么就是被遮住了
    var n = 1
    //console.log(this.cam.playMaxY)

    while (this.cam.floory + n * this.cam.pery <= this.cam.playMaxY) {
      var pos = new Vec3(
        this.node.position.x,
        this.node.position.y + n * this.cam.pery,
        this.node.position.z,
      )
      if (this.checkIfPeng(pos)) {
        return true
      }
      n++
    }
    return false
  }

  checkIfPeng(pos) {
    var perCeil = this.cam.perCeil
    var array = this.node.parent.children
    for (let index = 0; index < array.length; index++) {
      const element = array[index]
      if (
        Math.abs(element.position.y - pos.y) <= 0.1 &&
        !element.getComponent(coin).isFly
      ) {
        var v1 = new Vec2(pos.x, pos.z)
        var v2 = new Vec2(element.position.x, element.position.z)

        if (Vec2.len(v1.subtract(v2)) < perCeil) {
          //console.log(Vec2.len(v1.subtract(v2)))
          return element
        }
      }
    }
    return false
  }

  public getUnderPengNode() {
    var arr = []
    var pos = new Vec3(
      this.node.position.x,
      this.node.position.y - this.cam.pery,
      this.node.position.z,
    )
    var i = 0
    while (pos.y >= this.cam.floory) {
      pos = new Vec3(
        this.node.position.x,
        this.node.position.y - this.cam.pery * i,
        this.node.position.z,
      )
      var perCeil = this.cam.perCeil
      var array = this.node.parent.children
      for (let index = 0; index < array.length; index++) {
        const element = array[index]
        if (Math.abs(element.position.y - pos.y) <= 0.1) {
          var v1 = new Vec2(pos.x, pos.z)
          var v2 = new Vec2(element.position.x, element.position.z)

          if (Vec2.len(v1.subtract(v2)) < perCeil) {
            //console.log(Vec2.len(v1.subtract(v2)))
            arr.push(element)
          }
        }
      }
      i++
    }

    return arr
  }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
