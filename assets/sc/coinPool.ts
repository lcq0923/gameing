import { _decorator, Node, Component, Prefab, NodePool, instantiate } from 'cc'
import { coin } from './coin'

let { ccclass, property } = _decorator

/**采用节点池复用节点*/
@ccclass('coinPool')
export class coinPool extends Component {
  @property([Prefab])
  prefabs: (Prefab | null)[] = []
  private myPrefabs: any = {}
  onLoad() {
    for (let index = 0; index < this.prefabs.length; index++) {
      const element = this.prefabs[index]
      this.myPrefabs[element.name] = element
    }
    //console.log(this.myPrefabs)
  }

  //成员
  private Pools: any = {}

  createNode(name): any {

    let node = null
    if (!this.Pools[name]) this.Pools[name] = new NodePool()
    var isNew = false
    var Pool = this.Pools[name]
    if (Pool.size() > 0) {
      // 通过 size 接口判断对象池中是否有空闲的对象
      node = Pool.get()
    } else {
      // 如果没有空闲对象，也就是对象池中备用对象不够时，我们就用 cc.instantiate 重新创建
      node = instantiate(this.myPrefabs[name])
      node.name = name
      isNew = true
    }

    return { node, isNew }
  }
  destoryNode(n: Node) {
    var name = n.name
    if (!this.Pools[name]) this.Pools[name] = new NodePool()
      ; (this.Pools[name] as NodePool).put(n)
    n.getComponent(coin).onDestroy()
  }
}
