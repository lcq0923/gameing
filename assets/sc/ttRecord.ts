import { _decorator, Component, Node, director } from 'cc'
const { ccclass, property } = _decorator

@ccclass('ttRecord')
export class ttRecord extends Component {
  private red: any
  private redVideo: any

  recordStart() {
    var self = this
    if (typeof tt == 'undefined') {
      return false
    }
    var red = this.red || tt.getGameRecorderManager()
    var redVideo = this.redVideo

    // 监听录屏结束事件
    red.onStop((res) => {
      // 对录制完成的视频进行剪辑
      red.clipVideo({
        path: res.videoPath,
        timeRange: [30, 0],
        success(res) {
          // 由开始5秒 +最后10秒 拼接合成的视频
          console.log(res.videoPath)
          self.redVideo = res.videoPath
        },
        fail(e) {
          console.error(e)
        },
      })
    })
    red.onStart((res) => {
      // do somethine;
      console.log(res, '开始录屏')
    })
    red.start({
      duration: 300,
    })
  }
  recordStop() {
    var red = this.red || tt.getGameRecorderManager()
    red.stop()
  }

  share() {
    var redVideo = this.redVideo
    if (typeof tt != 'undefined') {
      if (redVideo) {
        console.log(redVideo)
        tt.shareAppMessage({
          channel: 'video',
          title: '哇！这游戏有点难！',
          desc: '#消除达人 最牛消除达人是你吗？',
          imageUrl: '',
          templateId: '', // 替换成通过审核的分享ID
          query: '',
          extra: {
            videoPath: redVideo, // 可替换成录屏得到的视频地址
            videoTopics: ['消除达人', '小游戏'],
          },
          success() {
            console.log('分享视频成功')
          },
          fail(e) {
            if (!/cancel/.test(e.errMsg))
              tt.showModal({
                title: '提示',
                content: '录屏时间太短无法分享1',
                showCancel: false,
              })
          },
        })
      } else
        tt.showModal({
          title: '提示',
          content: '录屏时间太短无法分享2',
          showCancel: false,
        })
    }
  }
}
