import { _decorator, Component, Node } from 'cc'
const { ccclass, property } = _decorator

@ccclass('set')
export class set extends Component {
  show() {
    this.node.active = true
  }
  hide() {
    this.node.active = false
  }

  update(deltaTime: number) {}
}
