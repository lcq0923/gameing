import { _decorator, Component, Node, instantiate, find, Label, assetManager, SpriteFrame, Sprite, Texture2D, ImageAsset, Color } from 'cc';
import { gb } from './gb';
const { ccclass, property } = _decorator;

@ccclass('rank')
export class rank extends Component {
    @property(Node)
    itemTmp


    show() {
        this.node.active = true
    }
    hide() {
        this.node.active = false
    }

    onLoad() {
        this.itemTmp.active = false
        var self = this
        gb.r({
            name: 'rank',
            success(e) {
                self.listInit(e)
            }
        })
    }
    listInit(data) {
        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            var item = instantiate(this.itemTmp)
            find('name', item).getComponent(Label).string = element.userName || '匿名'
            find('index', item).getComponent(Label).string = index + 1 + ''
            find('score', item).getComponent(Label).string = element.coinsNum
            find('bg', item).getComponent(Sprite).color = (index % 2 == 0 ? (new Color(255, 255, 255, 255)) : (new Color(235, 235, 235, 255)))
            if (element.avatorUrl) {
                ((element, item) => {
                    // assetManager.loadRemote(element.avatorUrl, { type: ImageAsset }, (err, e: ImageAsset) => {
                    //     console.log(err, e)
                    var e = new Image()
                    e.src = element.avatorUrl
                    e.onload = () => {
                        const tex = new Texture2D();

                        tex.reset({
                            width: 4,
                            height: 3
                        })

                        tex.mipmaps = [new ImageAsset(e)];
                        var s = new SpriteFrame()
                        s.texture = tex
                        find('face', item).getComponent(Sprite).spriteFrame = s
                    }
                    //})
                })(element, item)
            }

            item.active = true
            item.parent = this.itemTmp.parent
            //console.log(element);


        }
    }
}

