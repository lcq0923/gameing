import { _decorator, Component, Node, Label, tween } from 'cc'
const { ccclass, property } = _decorator

@ccclass('joAlert')
export class joAlert extends Component {
  @property(Label)
  titleLabel = new Label()
  @property
  _title = '提示'
  @property //金币get/set方法
  public get title() {
    return this._title
  }
  public set title(value) {
    this._title = value
    this.titleLabel.string = '' + value
  }
  @property(Node)
  vNode
  @property(Label)
  contentLabel = new Label()
  @property
  _content = '提示'
  @property //金币get/set方法
  public get content() {
    return this._content
  }
  public set content(value) {
    this._title = value
    this.contentLabel.string = '' + value
  }

  @property(Label)
  confirmTxtLabel = new Label()
  @property
  _confirmTxt = 'OK'
  @property //金币get/set方法
  public get confirmTxt() {
    return this._confirmTxt
  }
  public set confirmTxt(value) {
    this._confirmTxt = value
    this.confirmTxtLabel.string = '' + value
  }

  @property(Label)
  cancelTxtLabel = new Label()
  @property
  _cancelTxt = 'OK'
  @property //金币get/set方法
  public get cancelTxt() {
    return this._cancelTxt
  }
  public set cancelTxt(value) {
    this._cancelTxt = value
    this.cancelTxtLabel.string = '' + value
  }

  @property(Node)
  confirmBtn = new Node()
  @property(Node)
  cancelBtn = new Node()
  @property
  _needCancel = false
  @property //金币get/set方法
  public get needCancel() {
    return this._needCancel
  }
  public set needCancel(value) {
    this._needCancel = value
    this.cancelBtn.active = value
  }

  @property(Function)
  _onConfirm

  @property //金币get/set方法
  public get onConfirm() {
    return this._onConfirm
  }
  public set onConfirm(value: Function) {
    this._onConfirm = value
  }

  @property(Function)
  _onCancel

  @property //金币get/set方法
  public get onCancel() {
    return this._onCancel
  }
  public set onCancel(value: Function) {
    this._onCancel = value
  }

  shut() {
    this.node.active = false
  }
  private ifCancelShut = true
  private ifConfirmShut = true
  open(op: any) {
    this.ifCancelShut = true
    this.ifConfirmShut = true
    //tween(this.node.parent).delay(0.1).call(() => {
    this.title = op.title || '提示'
    this.content = op.content || ''
    this.onConfirm = op.confirm || (() => { })
    this.onCancel = op.cancel || (() => { })
    this.needCancel = op.needCancel || false
    this.cancelTxt = op.cancelTxt || 'CANCEL'
    this.confirmTxt = op.confirmTxt || 'OK'
    tween(this.node.parent).delay(0.2).call(() => {
      this.node.active = true
    }).start()

    if (typeof op.ifCancelShut != 'undefined')
      this.ifCancelShut = op.ifCancelShut

    if (typeof op.ifConfirmShut != 'undefined')
      this.ifConfirmShut = op.ifConfirmShut
    if (op.confirmBtnIsVideo) {
      if (this.vNode) this.vNode.active = true
    } else {
      if (this.vNode) this.vNode.active = false
    }
    //})
  }
  cancel(e) {
    this.onCancel(e, this)
    if (this.ifCancelShut) this.shut()
  }
  confirm(e) {
    if (this.onConfirm) this.onConfirm(e, this)
    if (this.ifConfirmShut) this.shut()
  }

  update(deltaTime: number) {
    this.cancelBtn.active = this._needCancel
  }
}
