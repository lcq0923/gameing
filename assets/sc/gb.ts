import { _decorator, Component, Node, assetManager, sys } from 'cc'
const { ccclass, property } = _decorator

@ccclass('gb')
export class gb extends Component {
  private static appName = typeof wx != 'undefined' ? 'wx' : 'pc'
  private static isVivo = false
  private static serverRoot = 'https://home.wxnodes.cn/JoLvWx/'
  /**
   * 视频广告封装
   * @param options
   * @returns
   */
  public static ad(options) {
    var gbSys
    if (typeof wx != 'undefined') gbSys = wx
    if (typeof tt != 'undefined') gbSys = tt
    if (typeof qg != 'undefined') gbSys = qg

    var act = options.success,
      cancel = options.cancel,
      before = options.before,
      loaded = options.loaded,
      after = options.after,
      error = options.fail
    if (typeof gbSys.createRewardedVideoAd == 'undefined') return

    console.log('ad Show2', options.adid)
    // var rewardedVideoAd
    // if (!gbSys.rewardedVideoAd) {
    if (typeof qg != 'undefined') {
      var rewardedVideoAd = gbSys.createRewardedVideoAd({ posId: options.adid })
    } else {
      var rewardedVideoAd = gbSys.createRewardedVideoAd({
        adUnitId: options.adid,
      })
    }
    //     gbSys.rewardedVideoAd = rewardedVideoAd
    // } else {
    //     rewardedVideoAd = gbSys.rewardedVideoAd
    // }

    rewardedVideoAd.load().then(function () {
      if (loaded) loaded()
      rewardedVideoAd.show()
    })

    if (before) before()

    var self = this
    function cfun(res) {
      if ((res && res.isEnded) || res === undefined) {
        if (act) act(rewardedVideoAd)
      } else {
        // 播放中途退出，不下发游戏奖励
        if (cancel) cancel()
      }

      rewardedVideoAd.offClose(cfun)
      if (after) after()
    }

    rewardedVideoAd.onClose(cfun)
    var efun = function (e) {
      if (error) {
        error()
      }
      console.log('广告错误', JSON.stringify(e))
      rewardedVideoAd.offError(efun)
      if (after) after()
    }
    rewardedVideoAd.onError(efun)

    return rewardedVideoAd
  }
  /**
   * 请求
   * @param options
   */
  public static r(options: any) {
    //var url = 'https://home.wxnodes.cn/JoXiguaWx/code2Session?code=073634100SpJFO1mjL0000shQZ16341Q'
    if (options.name) options.url = this.serverRoot + options.name
    if (options.noLogin) {
      this.Request(options)
    } else {
      this.RequestWithLogin(options)
    }
  }

  static Get(Url, header, cb) {
    let http = new XMLHttpRequest()
    http.open('GET', Url, true)
    if (header) {
      for (var i in header) {
        http.setRequestHeader(i, header[i])
      }
    }
    http.onreadystatechange = function (e) {
      //原生中e不存在
      if (
        e &&
        e.currentTarget.readyState == 4 &&
        e.currentTarget.status != 500
      ) {
        if (typeof cb == 'function') {
          cb(http.responseText)
        }
      } else {
        if (http.readyState == 4 && http.status != 500) {
          if (typeof cb == 'function') {
            cb(http.responseText)
          }
        }
      }
      if ((e && e.currentTarget.status == 500) || http.status == 500) {
        console.log(
          '服务器错误',
          http.responseText.replace(/<style[^>]*?>[\s\S]*?<\/style>/, ''),
        )
      }
    }
    http.timeout = 10000
    http.send()
  }
  static Post(Url, data, header, cb) {
    data = JSON.stringify(data) //以前不懂要怎么传，是缺少这一步
    let http = new XMLHttpRequest()
    http.open('POST', Url, true)
    if (header) {
      for (var i in header) {
        http.setRequestHeader(i, header[i])
      }
    }
    http.onreadystatechange = function (e) {
      console.log('http.readyState', http.readyState, http.status)
      if (e && http.readyState == 4 && http.status != 500) {
        if (typeof cb == 'function') {
          cb(http.responseText)
        }
      } else {
        if (http.readyState == 4 && http.status != 500) {
          if (typeof cb == 'function') {
            cb(http.responseText)
          }
        }
      }
      if ((e && http.readyState == 500) || http.status == 500) {
        console.log(
          '服务器错误',
          http.responseText.replace(/<style[^>]*?>[\s\S]*?<\/style>/, ''),
        )
      }
    }
    http.timeout = 10000 //超时10秒
    http.send(data)
  }
  /**
   * 原生请求
   * @param op
   */
  static Request(op: any) {
    if (op.method == 'post') {
      console.log(
        'post请求开始',
        op.url,
        JSON.stringify(op.data),
        JSON.stringify(op.header),
      )
      this.Post(op.url, op.data, op.header, function (e) {
        try {
          var d = JSON.parse(e)
          op.success({
            statusCode: 200,
            data: d,
          })
        } catch (error) {
          console.warn(error)
        }

        //} else {
        //     op.success({
        //         statusCode:200,
        //         data: e
        //     })
        // }
      })
    } else {
      console.log('get请求开始')
      this.Get(op.url, op.header, function (e) {
        console.log(e, 'getget')
        try {
          var d = JSON.parse(e)
          op.success({
            statusCode: 200,
            data: d,
          })
        } catch (error) {
          console.warn(error)
        }
        // } else {
        //     op.success({
        //         statusCode:200,
        //         data: e
        //     })
        // }
      })
    }
  }
  static Login(op: any) {
    var self = this
    console.log(op)

    /**
     * 兼容小游戏平台的login
     * @param op
     */
    var gbLogin = function (op) { }
    if (typeof wx != 'undefined') {
      gbLogin = wx.login
    } else {
      gbLogin = function (op) {
        op.success({
          code: 'debugToken',
        })
      }
    }

    console.log('login', op.name)

    gbLogin({
      success(res) {
        if (self.appName == 'qg') {
          var to = res.data.token
        } else {
          var to = res.code
        }
        //console.log('登录成功',res.data.token,to)
        if (to) {
          var url = self.serverRoot + 'code2Session?code=' + to

          //ghost模式
          var ghostId = null
          //直接电脑浏览器上的话，用ghost模式
          if (self.appName == 'pc' && typeof jsb == 'undefined') {
            function gv(variable) {
              if (!window.location.search) return false
              var query = window.location.search.substring(1)
              var vars = query.split('&')
              for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=')
                if (pair[0] == variable) {
                  return decodeURI(pair[1])
                }
              }
              return false
            }
            ghostId = gv('ghostId') || 'oQJaf4t9ckXHDJprWcsolMcPUYks' //在链接上加ghostId=openid即可模拟
            url += '&ghostId=' + ghostId
          }

          self.Request({
            url: url,
            type: 'json',
            success(e) {
              //console.log('aaa', JSON.stringify(e))
              if (e.data && e.data.success) {
                self.setJokey(e.data.key)

                if (typeof op.success == 'function') {
                  op.success(e)
                }
              } else {
                //console.log('aaafff2')
                if (typeof op.fail == 'function') {
                  op.fail(e)
                }
              }

              //cc.sys.localStorage.setItem('jokey')
            },
            fail(e) {
              //console.log('aaafff', e)
              if (typeof op.fail == 'function') {
                op.fail(e)
              }
            },
          })
          console.log('请求后')
        } else {
          if (typeof op.fail == 'function') {
            op.fail()
          }
        }
      },
      fail(e) {
        console.log('取消登录，以游客身份登录')
        self.Request({
          url: self.serverRoot + 'youke',
          type: 'json',
          success(e) {
            if (e.data && e.data.success) {
              self.setJokey(e.data.key)
              if (typeof op.success == 'function') {
                op.success(e)
              }
            } else {
              if (typeof op.fail == 'function') {
                op.fail(e)
              }
            }

            //cc.sys.localStorage.setItem('isYouke', 1)
            //GameGlobal.isYouke=e.data.key

            self.setIsYouke(1)
          },
          fail(e) {
            if (typeof op.fail == 'function') {
              op.fail(e)
            }
          },
        })
      },
    })
  }
  /**
   *
   * @param op ifForceLogin 是否每次都要调起登录
   * @param op ifNoLoginCheck 是否检查登录
   */
  static RequestWithLogin(op) {
    var self = this

    if (op.ifForceLogin) {
      sys.localStorage.removeItem('jokey')
      op.ifForceLogin = false
    }
    //如果没有key，先login
    //if (!cc.sys.localStorage.getItem('jokey')) {
    //if(!GameGlobal.jokey){
    if (!self.getJokey() && !op.ifNoLoginCheck) {
      console.log('c1')
      // if(GameGlobal.gbSys.appName=='qg'){
      //     request(op)
      // }else
      self.Login({
        success(e) {
          console.log('登录login')
          self.RequestWithLogin(op)
        },
        fail(e) {
          console.log('登录错误', e)
          if (typeof op.fail == 'function') {
            op.fail('登录错误', e)
          }
        },
      })
    } else {
      //有key了
      if (!op.data) {
        op.data = {}
      }
      op.data.joKey = self.getJokey()
      // console.log('有jokey的请求', op.data.joKey)
      //ghost模式
      if (this.appName == 'pc' && typeof jsb == 'undefined') {
        console.log('有jokey的请求1')
        function gv(variable) {
          if (!window.location.search) return false
          var query = window.location.search.substring(1)
          var vars = query.split('&')
          for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=')
            if (pair[0] == variable) {
              return decodeURI(pair[1])
            }
          }
          return false
        }
        op.data.ghostId = gv('ghostId') || 'oQJaf4t9ckXHDJprWcsolMcPUYks' //在链接上加ghostId=openid即可模拟
      }
      console.log('有jokey的请求2')
      var u = op.url

      var ops = {
        url: u,
        method: 'post',
        type: 'json',
        data: op.data,
        success(e) {
          console.log('有jokey的请求3')
          //console.log(JSON.stringify(e), u, 'serverRequest')
          if (e.statusCode != 200) {
            if (typeof op.fail == 'function') {
              op.fail('错误3', e)
            }
            return
          }
          if (e.tempFilePath)
            e.data = JSON.parse(
              wx.getFileSystemManager().readFileSync(e.tempFilePath, 'utf8'),
            )
          if (typeof op.success == 'function') {
            op.success(e.data)
          }
        },
        fail(e) {
          console.log(e, '错误5')
          if (typeof op.fail == 'function') {
            op.fail('错误4', e)
          }
        },
      }
      ops.fail = function (e) {
        console.log(e, 'tetete')
      }

      //vivo兼容
      if (self.isVivo) {
        ; (ops as any).header = {
          'Content-Type': 'application/json',
        }
        ops.data = JSON.stringify(op.data)
      }
      console.log('有jokey的请求4')
      self.Request(ops)
    }
  }

  static setJokey = function (v) {
    return sys.localStorage.setItem('jokey', v)
    //GameGlobal.jokey=e.data.key
  }
  static removeJokey = function () {
    return sys.localStorage.removeItem('jokey')
    //GameGlobal.jokey=false
  }
  static getJokey = function () {
    return sys.localStorage.getItem('jokey')
  }

  static getIsYouke = function () {
    return sys.localStorage.getItem('isYouke')
  }

  static setIsYouke = function (v) {
    return sys.localStorage.setItem('isYouke', v)
  }

  static removeIsYouke = function () {
    return sys.localStorage.removeItem('isYouke')
  }
  static loginOut = function () {
    sys.localStorage.removeItem('isYouke')
    sys.localStorage.removeItem('jokey')
  }
  /**
   * wx更新远程用户头像昵称数据
   * @param cb
   */
  static updateUserFace = function (cb, res = false) {

    var self = this
    var act = (res) => {
      var userInfo = res.userInfo
      var nickName = userInfo.nickName
      var avatarUrl = userInfo.avatarUrl
      var gender = userInfo.gender //性别 0：未知、1：男、2：女
      var province = userInfo.province
      var city = userInfo.city
      var country = userInfo.country
      self.r({
        name: 'saveFaceName',
        data: {
          userName: nickName || '游客',
          avatorUrl: avatarUrl || '',
          province: province,
          city: city,
        },
      })
      cb({
        userName: nickName || '游客',
        avatorUrl: avatarUrl || '',
        province: province,
        city: city,
      })
    }
    // 必须是在用户已经授权的情况下调用
    if (typeof wx != 'undefined' && typeof wx.getUserInfo != 'undefined') {
      if (res) {
        act(res)
      } else
        wx.getUserInfo({
          success: function (res) {
            act(res)
          },
          fail(e) {
            console.log('用户信息获取失败', e)
          },
        })
    }
    else cb({})
  }
}
