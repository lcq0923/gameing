import { _decorator, Component, Node, director, sys } from 'cc'
const { ccclass, property } = _decorator

@ccclass('end')
export class end extends Component {
  click() {
    sys.localStorage.setItem('nowQuessIndex', '0')
    director.loadScene('scene')
  }

  update(deltaTime: number) {}
}
