import {
  game,
  _decorator,
  Component,
  Node,
  assetManager,
  log,
  director,
  Label,
  ProgressBar,
} from 'cc'
import { gb } from './gb'
const { ccclass, property } = _decorator

@ccclass('loading')
export class loading extends Component {
  @property(Label)
  txt
  @property(Label)
  bartxt
  @property(ProgressBar)
  bar
  @property(ProgressBar)
  totalbar
  @property
  frame = 30
  @property
  timeout = 20000
  onLoad() {
    if (this.frame < 60) game.frameRate = this.frame || 30
    // var isAndroid = false
    // if (typeof navigator != 'undefined') {
    //   var u = navigator.userAgent;
    //   isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
    // }
    // console.log(isAndroid);

    // if (!isAndroid) {
    //   this.bar.node.active = false
    //   this.txt.node.active = false
    // }

    var self = this
    var sceneId = 'scene'
    var hasGo = false
    var isAndroid = false
    if (typeof navigator != 'undefined') {
      var u = navigator.userAgent
      isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1
    }
    console.log(isAndroid)

    if (false) {
      director.loadScene(sceneId)
    } else {
      //最多15秒
      var timeout = setTimeout(() => {
        if (!hasGo) {
          hasGo = true
          director.loadScene(sceneId)
        }
      }, this.timeout)
      assetManager.loadBundle(
        'resources',
        {
          onFileProgress: function (arg0, a) {
            console.log(
              '正在加载场景 sceneId: ' +
                sceneId +
                'arg0: ' +
                JSON.stringify(arg0),
            )
            //arg0 = {"totalBytesWritten":31707,"totalBytesExpectedToWrite":32850,"progress":96,"cookies":[]}
          },
        },
        (err, bundle) => {
          if (hasGo) {
            self.txt.string = '100%'
            return
          }

          var array = ['fbx', 'coins', 'sprite', 'mp3', 'prefabs', 'json']

          function m(index, cb) {
            if (hasGo) {
              return
            }
            var name = array[index]
            if (!name) {
              if (cb) cb(index)
              return
            }
            bundle.loadDir(
              name + '/',
              (f, total, item) => {
                if (hasGo) {
                  return
                }
                var ni = ''
                switch (name) {
                  case 'json':
                    ni = '数据'
                    break
                  case 'coins':
                    ni = '元素'
                    break
                  case 'fbx':
                    ni = '模型'
                    break
                  case 'mp3':
                    ni = '音效'
                    break
                  case 'prefabs':
                    ni = '预制'
                    break
                  case 'sprite':
                    ni = '图片'
                    break
                  default:
                    break
                }
                self.bartxt.string = ni
                self.bar.progress = f / total
                var per = 1 / (array.length + 1)
                self.totalbar.progress = per * index + (f * per) / total
                self.txt.string =
                  (100 * per * index + (f * per * 100) / total).toFixed(0) + '%'
              },
              (err) => {
                //console.log(index)
                if (hasGo) {
                  return
                }
                m(index + 1, cb)
              },
            )
          }
          m(0, (index) => {
            bundle.loadScene(
              sceneId,
              (f, total) => {
                self.bartxt.string = '场景'
                var per = 1 / (array.length + 1)
                self.bar.progress = f / total
                self.totalbar.progress = per * index + (f * per) / total
                self.txt.string =
                  (100 * per * index + (f * per * 100) / total).toFixed(0) + '%'
              },
              function (err, scene) {
                clearTimeout(timeout)
                if (!hasGo) {
                  hasGo = true
                  self.txt.string = '100%'
                  director.runScene(scene)
                }
              },
            )
          })
        },
      )
    }
  }

  update(deltaTime: number) {}
}
